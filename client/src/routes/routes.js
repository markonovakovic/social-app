import Profile from '../pages/Profile/UserProfile';
import Home from '../pages/home';
import Groups from '../pages/Groups/Groups';
import Login from '../pages/login';
import Signup from '../pages/signup';
import GroupLanding from '../pages/groupLanding';
import Friends from '../pages/SuggestedFriends/SuggestedFriends';
import MyProfile from '../pages/Profile/MyProfile';

 const routes = [
    {
        path:'/',
        component:Home,
        protected: false
    },
    {
        path:'/groups',
        component:Groups,
        protected: true
    },
    {
        path:'/groups/:name',
        component:GroupLanding,
        protected: true
    },
    {
        path:'/friends',
        component:Friends,
        protected: true
    },
    {
        path:'/login',
        component:Login,
        protected: false
    },
    {
        path:'/signup',
        component:Signup,
        protected: false
    },
    {
        path:'/profile/:slug',
        component:Profile,
        protected: true
        
    },
    {
        path:'/my-profile',
        component:MyProfile,
        protected: true
        
    }
]

export default routes;