
import { createMuiTheme } from '@material-ui/core/styles';
import { green, amber, blue } from '@material-ui/core/colors' //eslint-disable-line

const theme =  createMuiTheme({
    palette:{
        primary:{
            main:green['400']
        },
        
        secondary:{
            main:amber[300]
        }
    },
    overrides:{
        MuiCardContent:{
        
            root:{
                paddingBottom:'0px !important',
              
                
            }
        },
        MuiInputBase:{
            formControl:{
                
            },
            input:{
                height:"0.6rem",
                padding:'13px'
            }
        }
    }
});

export default theme;