import {
    GET_POSTS_BY_USER,
    SET_LIKE_POST,
    POST_ERROR,
    CLEAR_POSTS
} from '../types';

const defaultPagination = {
    page:0,
    pages:0,
    pages_total:0,
    perpage:0
}
const initialState = {
    posts:[],
    posts_total:0,
    post:null,
    loading:true,
    error:{},
    pagination:{ ...{}, ...defaultPagination }

}
export default function(state = initialState, action) {
   const { type, payload} =  action;
    switch(type) {
        case GET_POSTS_BY_USER:
            const { posts, pages, page, pages_total, perpage, posts_total} = payload;
            return {
                ...state,
               posts: posts,
               pagination: {...state.pagination,  pages, page, pages_total, perpage},
               posts_total:posts_total,
               loading:false,
            }
        case SET_LIKE_POST: 

            return {
                ...state,
                posts:state.posts.map(post => {
                    const index = post.likes.findIndex(item => item._id === String(payload.user._id));
                    if(post._id === payload.postId) {
                        if(index !== -1) {
                            return   {...post, likes: post.likes.filter(like => like._id !== payload.like)}
                        } else {
                             
                            const newLikes =  { _id:payload.user._id, firstName:payload.user.firstName, lastName:payload.user.lastName }
                            return   {...post, likes: [...post.likes, newLikes]}
                        } 
                   }
                   return post;
                })
            }
            
        case CLEAR_POSTS: 
           return {
               ...state,
               posts:[],
               post:null,
               loading:true,
               pagination: {...state.pagination, ...defaultPagination}
           }
        case POST_ERROR:
            return {
                ...state,
                posts:[],
                post:null,
                error: {...state.error, greska:'nesto ne valja'},
                loading:false,
                pagination: {...state.pagination, defaultPagination}
            }
        default:
                return state;
        }
    
};
