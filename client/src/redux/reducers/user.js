
import {
    GET_USER,
    USER_ERROR,
    GET_GROUPS_BY_USER,
    UPDATE_USER_PROFILE,
    GET_FRIENDS_BY_USER,
    WITHDRAW_JOIN_GROUP_REQUEST,
    SEND_FRIENDSHIP_REQUEST,
    GROUP_ERROR,
    SUGGESTED_FRIENDS,
    FRIENDS_ERROR

} from '../types';


const initialState = {
    users:[],
    user:null,
    groups:[],
    friends:[],
    friends_suggested:[],
    posts_total:0,
    loading:true,
    error:{},
    loadingFriends:true,
    pagination:{
        page:0,
        pages:0,
        pages_total:0,
        perpage:0
    }

}
export default function(state = initialState, action) {
    const { type, payload} =  action;
     switch(type) {
         case GET_USER:
        
             return {
                 ...state,
                user: payload,
                loading:false,
             }
            case USER_ERROR: 
              return {
                  ...state,
                  users:[],
                  user:null,
                  loading:true,
              }
            case GET_GROUPS_BY_USER: 
              return {
                ...state,
                groups:[...payload.groups],
                error:{}
              }
            case UPDATE_USER_PROFILE: 
               return {
                   ...state,
                   user: payload,
                   loading:false
               }
            case GET_FRIENDS_BY_USER: 
               return {
                   ...state,
                   friends: payload,
                   loading:false,
                   loadingFriends:false,
               }
            case WITHDRAW_JOIN_GROUP_REQUEST: 
                return {
                    ...state,
                    groups: state.groups.map(item => {
                        if(item.group._id === payload.group) {
                          delete payload.group;
                          return item.status === 'requested' ?   {...item, status:null} :  {...item, ...payload} ;
                        
                        }
                        return item;
                      })
                }
            case SUGGESTED_FRIENDS:
                const { suggested_friends, page, pages, pages_total, perpage} = payload;
                 return {
                     ...state,
                     friends_suggested: suggested_friends,
                     loadingFriends:false,
                     pagination: {
                         ...state.pagination,
                         page,
                         pages,
                         pages_total,
                         perpage
                        }
                 }
            case SEND_FRIENDSHIP_REQUEST: 
            
            const {slug, requestType} = payload;
                return {
                    ...state,
                     friends_suggested: state.friends_suggested.map(friend => {
                         console.log(friend);
                        if(friend.user.slug === slug) {
                            switch(requestType) {
                               case 'add':
                                   console.log(slug)
                                   const status = friend.status === null ? 'requested': null;
                                   return { ...friend, status:'requested' }
                                case 'withdraw':
                                    return { ...friend, status: null }
                                default: 
                                  return friend;
                           }  
                        }
                        return friend
                    }),
                    friends: state.friends.map(friend => {
                        console.log(friend);
                       if(friend.user.slug === slug) {
                            switch(requestType) {
                                case 'add':
                                    return { ...friend, status:'requested' }
                                case 'withdraw':
                                case 'reject':
                                case 'remove':
                                   return { ...friend, status: null }
                                case 'accept':
                                    return { ...friend, status: 'accepted' }
                               default: 
                                 return friend;
                          }  
                       }
                       return friend
                   }) 
                }
            case FRIENDS_ERROR:
                return  {
                    ...state, 
                    friends:[],
                    loadingFriends:false,
                    loading:false
                }
            case GROUP_ERROR:
                return {
                    ...state,
                    groups:[],
                    loading:false,
                    error:{...state.error,message: payload}
                }
            
         default: 
            return state
     }
 };
 