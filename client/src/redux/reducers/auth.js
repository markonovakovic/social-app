import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    USER_LOADED,
    AUTH_ERROR,
    UPDATE_AUTH_USER,
    LOGIN_SUCCESS,
    LOGOUT
} from '../types';

const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated:JSON.parse(localStorage.getItem('auth')) || false,
    loading: true,
    success:false,
    user: null
};


export default function(state = initialState, action) {
    const { type, payload }  = action;
    switch(type){
        case USER_LOADED:
            localStorage.setItem('auth', true);
            return  {
                ...state,
                isAuthenticated: true, 
                loading:false,
                user:payload
            }
        case REGISTER_SUCCESS:
            return {
                ...state, 
                loading:false,
                success:true
            }
        case LOGIN_SUCCESS:
            localStorage.setItem('token', payload.token);
            return {
                ...state,
                ...payload,
                isAuthenticated: true,
                loading:false,
                success:true
            }
        case UPDATE_AUTH_USER: 
            return {
                ...state,
                user: payload,
                loading:false
            }
        case REGISTER_FAIL:
        case AUTH_ERROR:
        case LOGOUT:
            localStorage.removeItem('token');
            localStorage.removeItem('auth');
            return {
                ...state, 
                token: null,
                isAuthenticated:false,
                user:null,
                loading: true,
                success:false
            }
        default:
            return state;

    }
}
