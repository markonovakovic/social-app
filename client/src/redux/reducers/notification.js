import {
     GET_UNREAD_NOTIFICATIONS, 
     NOTIFICATIONS_ERROR,
     SET_NOTIFICATION_COUNT,
     NOTIFICATION_HAS_SEEN,
     CLEAR_NOTIFICATIONS
     } from '../types';

const initialState = {
   notifications:[],
   notificationCount:0,
   error:null,
   loading:true,
};


export default function(state = initialState, action) {
    const { type, payload }  = action;
    switch(type){
        case GET_UNREAD_NOTIFICATIONS:
           return {
               ...state,
               notifications:[...payload],
               notificationCount: payload.filter(notification => notification.isSeen === false).length,
               loading:false
           }
        case NOTIFICATIONS_ERROR:
            return {
                ...state,
                error:payload,
                loading:false
            }
        case SET_NOTIFICATION_COUNT:
            return {
                ...state,
                notificationCount:payload
            }
        case NOTIFICATION_HAS_SEEN:
            return {
                ...state,
                notifications: state.notifications.map(notification => {
                    if(notification._id === payload._id) {
                        return {...payload}
                    }
                    return notification;
                })
            }
        case CLEAR_NOTIFICATIONS:
            return {
                ...state,
                notifications:[],
                notificationCount:0
            }
        default:
            return state;
    }
}