import { combineReducers } from 'redux';
import alertReducer from './alert';
import authReducer from './auth';
import postReducer from './post';
import userReducer from './user';
import groupReducer from './group';
import notificationReducer from './notification';

export default combineReducers({
    alerts:alertReducer,
    auth:authReducer,
    post:postReducer,
    user: userReducer,
    group: groupReducer,
    notification: notificationReducer
})