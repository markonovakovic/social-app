import {
  GROUPS_SUGGESTED,
  SEND_JOIN_GROUP_REQUEST,
  GROUP_ERROR
} from '../types';


const initialState = {
    suggested:[],
    group:null,
    loading:true,
    errors: {}
}

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch(type) {
    case GROUPS_SUGGESTED: 
      return {
        ...state,
        suggested:[...payload],
        loading:false,
        groups:null
      }
    case GROUP_ERROR:
        return {
            ...state,
            groups:[],
            loading:false,
        }
    case SEND_JOIN_GROUP_REQUEST: 
      return {
        ...state,
        suggested: state.suggested.map(item => {
          if(item.group._id === payload.group) {
            delete payload.group;
            return item.status === 'requested' ?   {...item, status:null} :  {...item, ...payload} ;
          
          }
          return item;
        })
      }
    default: 
       return state;
  }
}