import axios from '../../config/axiosConfig';
import { setAlert } from './alert'
import {  getUnreadNotifications, clearNotifications } from './notification';

import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  LOGIN_SUCCESS,
  UPDATE_AUTH_USER,
  LOGIN_FAIL,
  AUTH_ERROR,
  CLEAR_NOTIFICATIONS
} from '../types';
import setAuthToken from '../../utils/setAuthToken';
//Load User
export const loadUser = () => async dispatch => {
  
    
    try {
        if(localStorage.token) {
            setAuthToken(localStorage.token);
            let result = await axios.get('/api/auth/whoaim');
            localStorage.setItem('auth', true);
            dispatch(getUnreadNotifications());
            dispatch({
                type:USER_LOADED,
                payload: result.data
            })
        } else {
            localStorage.removeItem('auth');
        } 
    } catch (error) {
        dispatch({
            type: AUTH_ERROR
        });
    }
}
//Register user
export const register = (data) => async dispatch => {

    delete data.password2
    const response = await axios.post('/api/users/create', data);
    try {
        dispatch({
            type:REGISTER_SUCCESS,
            payload: response.data
        })
        
    } catch (err) {
        const errors = err.response.data.errors;
        if (errors) {
            errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type:REGISTER_FAIL
        })
    
     }
}

    export const login = (data) => async dispatch => {
       
      
        try {
            const response = await axios.post('/api/auth', data);
           await dispatch({
                type:LOGIN_SUCCESS,
                payload: response.data
            });
            loadUser();
            dispatch(loadUser());
            
        } catch (err) {
           dispatch(setAlert(err.response.data, 'danger',3500));
           /*  if (errors) {
                errors.forEach(error => dispatch(setAlert(error.msg, 'danger', 3000)));
            } */
    
            dispatch({
                type:LOGIN_FAIL
            });
        }
} 


export const updateAuthUser = data => async dispatch => {
   
    try {
        const response = await axios.patch(`/api/users/update`,data);
        console.log(response);
        dispatch({
            type:UPDATE_AUTH_USER,
            payload: response.data
        })
        dispatch({type:USER_LOADED, payload:response.data});
    } catch (error) {
        dispatch({
            type:AUTH_ERROR
        })
    }

};