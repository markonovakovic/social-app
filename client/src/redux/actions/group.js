import axios from '../../config/axiosConfig';

import { 
  GROUPS_SUGGESTED,
  SEND_JOIN_GROUP_REQUEST,
  GROUP_ERROR
} from '../types';



export const fetchSuggestedGroup= () => async dispatch => {

    try {
        const response = await axios.get(`/api/groups/suggested`);
        dispatch({
            type:GROUPS_SUGGESTED,
            payload: response.data
        })
    } catch (error) {
        dispatch({
            type: GROUP_ERROR
        })
    }
}

export const sendJoinRequest = (id) => async dispatch => {
    try {
        const response = await axios.post(`/api/groups/${id}/request`);
        dispatch({
             type:SEND_JOIN_GROUP_REQUEST,
             payload:response.data
        })
    } catch (error) {
        dispatch({
           type: GROUP_ERROR
        })
    }
}