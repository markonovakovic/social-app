import axios from '../../config/axiosConfig';

import {
    GET_USER,
    USER_ERROR,
    GET_GROUPS_BY_USER,
    USER_LOADED,
    GET_FRIENDS_BY_USER,
    UPDATE_USER_PROFILE,
    WITHDRAW_JOIN_GROUP_REQUEST,
    SEND_FRIENDSHIP_REQUEST,
    GROUP_ERROR,
    FRIENDS_ERROR,
    SUGGESTED_FRIENDS
} from '../types';

export const getUser = slug => async dispatch => {

    try {
        const result = await axios.get(`/api/users/${slug}`);
        dispatch({
            type:GET_USER,
            payload: result.data
        })
    } catch (error) {
        dispatch({
            type:USER_ERROR,
        });

    }
}

// fetch groups that belong certain user
export const fetchGroups = slug => async dispatch => {

    try {
        const response = await axios.get(`/api/users/${slug}/groups`);
        dispatch({
            type:GET_GROUPS_BY_USER,
            payload: response.data
        })
    } catch (error) {
        console.error(error.response.data);
        dispatch({
            type: GROUP_ERROR,
            payload:error.response.data
        })
    }
}


export const updateUser = data => async dispatch => {
   
    try {
        const response = await axios.patch(`/api/users/update`,data);
        console.log(response);
        dispatch({
            type:UPDATE_USER_PROFILE,
            payload: response.data
        })
       const  {_id, slug, firstName, lastName, email, isActive, avatar, phone, bio} =  response.data
        dispatch({
            type:USER_LOADED,
             payload:  {_id, slug, firstName, lastName, email, isActive, avatar, phone, bio} 
        });
    } catch (error) {
        dispatch({
            type:USER_ERROR
        })
    }

};

export const withdrawJoinGroupRequest = id => async dispatch => {
    try {
        const response = await axios.post(`/api/groups/${id}/request`);
        console.log(response.data);
        dispatch({
             type:WITHDRAW_JOIN_GROUP_REQUEST,
             payload:response.data
        })
    } catch (error) {
        dispatch({
           type: USER_ERROR
        })
    }
}


export const fetchFriendsByUser =(slug, status='all')=> async dispatch => {
   
    try {
        const response = await axios.get(`/api/users/${slug}/friends?status=${status}`);
        dispatch({
             type:GET_FRIENDS_BY_USER,
             payload:response.data
        })
    } catch (error) {
        dispatch({
           type: FRIENDS_ERROR
        })
    }

}

export const fetchSuggestedFriends = (params) => async dispatch => {

    //api/suggested-friends
    try {
        const response = await axios.get(`/api/suggested-friends`);
        dispatch({
             type:SUGGESTED_FRIENDS,
             payload:response.data
        })
    } catch (error) {
        dispatch({
           type: FRIENDS_ERROR
        })
    }
}

export const sendFriendshipRequest  =(slug, requestType)=> async dispatch => {
    
    try {
        await axios.patch(`/api/users/friendship/${slug}/request`);
        dispatch({
             type:SEND_FRIENDSHIP_REQUEST,
             payload:{slug, requestType}
        })
    } catch (error) {
        dispatch({
           type: FRIENDS_ERROR
        })
    }
  
}