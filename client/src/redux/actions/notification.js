import axios from '../../config/axiosConfig';
import {
    GET_UNREAD_NOTIFICATIONS,
    NOTIFICATIONS_ERROR,
    NOTIFICATION_HAS_SEEN,
    CLEAR_NOTIFICATIONS
} from '../types';


export const getUnreadNotifications = () => async dispatch => {
   
    try {
        const result = await axios.get(`/api/notifications/unread`);
        console.log(result.data);
        dispatch({
            type:GET_UNREAD_NOTIFICATIONS,
            payload: result.data
        })
    } catch (error) {
        console.log('notification error', error);
        dispatch({
            type:NOTIFICATIONS_ERROR,
        });

    }
}

export const clearNotifications = () => async dispatch => {
    dispatch({
        type:CLEAR_NOTIFICATIONS,
    })
}
export const setNotificationStatus = (id)=> async dispatch => {
    try {
         const result = await axios.patch(`/api/notifications/${id}/seen`);

         dispatch({
            type:NOTIFICATION_HAS_SEEN,
            payload:result.data.notification
        });
        
    } catch (error) {
        console.log('notification error', error);
        dispatch({
            type:NOTIFICATIONS_ERROR,
        });
    }
}