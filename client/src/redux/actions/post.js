import axios from '../../config/axiosConfig';
import {
    GET_POSTS_BY_USER,
    SET_LIKE_POST,
    POST_ERROR,
    CLEAR_POSTS
} from '../types';

export const getPostsByUser = slug => async dispatch => {
   
    try {
        const result = await axios.get(`/api/users/${slug}/posts`);
        dispatch({
            type:GET_POSTS_BY_USER,
            payload: result.data
        })
    } catch (error) {
        console.log('post error', error);
        clearPosts()
        dispatch({
            type:POST_ERROR,
        });

    }
}

// if post has alredy been liked post, the post will be unliked
export const likePostToggle = (id, authUser) => async dispatch => {
    try {
        const result = await axios.post(`/api/posts/${id}/like`);
        const data = {...result.data, postId: id, user: authUser }
        dispatch({
            type:SET_LIKE_POST,
            payload: data
        })
    } catch (error) {
        console.log('post error', error);
        clearPosts()
        dispatch({
            type:POST_ERROR,
        });

    }
}
export const clearPosts = () => dispatch => {
    dispatch({
        type:CLEAR_POSTS
    })
}