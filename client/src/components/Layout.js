import React, {Fragment} from 'react'
import Navbar  from './Navbar/Navbar'
import Container from '@material-ui/core/Container';

 const Layout = (props) => {
    return (
        <Fragment>
            
            <Navbar />
            <div className="main" style={{background:'#f5f5f5'}}> 
                
                <Container maxWidth="md" style={{ maxWidth:'1000px', minHeight: '93vh' }}>
                   {props.children}
                   
                </Container>
            </div>
            
        </Fragment>
    )
}

export default Layout;