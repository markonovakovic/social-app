import React,{ Fragment} from 'react';
import { connect } from 'react-redux';
import { getUnreadNotifications, setNotificationStatus } from '../../redux/actions/notification';
import { makeStyles } from '@material-ui/core/styles';
import NotificationItem from './NotificationItem';

import {
  List,
  ListItem,
  ListItemText,
  Divider
} from '@material-ui/core';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
//import ListItemText from '@material-ui/core/ListItemText';
/* import Divider from '@material-ui/core/Divider';
import InboxIcon from '@material-ui/icons/Inbox';
import DraftsIcon from '@material-ui/icons/Drafts'; */

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 560,
    minWidth: 300,
    backgroundColor: theme.palette.background.paper,
  },
}));


 function SimpleList({ notifications, error, loading,setNotificationStatus}) {
  
  const classes = useStyles();
  const notificationClickHandler = id => {
    console.log('seen tralala0', id);
    setNotificationStatus(id);
  }
  return (
    <div className={classes.root}>
      <List component="nav" aria-label="secondary mailbox folders">
        {notifications.length > 0 && notifications.map((notification, index) => {
            return (
            <Fragment key={index}>
              <NotificationItem notification={notification}  notificationClickHandler={notificationClickHandler} />
              <Divider />
            </Fragment>
            )
        })}
        
      </List>
    </div>
  );
};
const mapStateToProps = state => ({
  notifications: state.notification.notifications,
  error: state.notification.error,
  loading:state.notification.loading
});
 

export default connect(mapStateToProps,{setNotificationStatus})(SimpleList);
