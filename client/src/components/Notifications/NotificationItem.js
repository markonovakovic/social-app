import React from 'react'
import { useHistory } from "react-router-dom";
import {
    makeStyles,
    ListItem,
    ListItemText,
    Button 
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      maxWidth: 560,
      minWidth: 300,
      backgroundColor: 'blue',
    },
    margin: {
        margin: '2px',
      },
    item: {
        marginBottom:'1px',
        '&:hover': {
            backgroundColor: 'green'
        }
      /*   backgroundColor: 'rgb(237, 242, 250)', */
      },
  }));
  

 const NotificationItem = ({notification, notificationClickHandler}) => {
    const classes =  useStyles();
    let history = useHistory();
 
    const clickHandler = (type) => {
        console.log('click', type);
        notificationClickHandler(notification._id);
        //redirectTo(type)
    }

    const redirectTo =(type) => {
        switch(type) {
            case 'standard':
                history.push('/')
               break;
            case 'accepted_friendship':
                history.push('/my-profile?tab=3')
                break;
        }
    }
   
    const bgColor = notification.isSeen ? '#fff':'#e9ebee'
    switch(notification.type) {
      /*   case 'request_friendship':
            return (
                <ListItem button classes={classes.root}>
                    <ListItemText primary={notification.message} />
                    {'accepted_friendship'}
                </ListItem>
            )
        case 'accepted_friendship':
            return (
                <ListItem button classes={classes.root}>
                    <ListItemText primary={notification.message} />
                    {'accepted_friendship dfgdfgfgdgdggfdg'}
                </ListItem>
            ); */
        case 'accept_new_member':
            return (
                <ListItem className={classes.item} style={{backgroundColor : bgColor} }>
               <ListItemText primary={notification.message} />
              {/*  <CheckCircleOutlineIcon color="primary" fontSize="large" onClick={() => console.log('accept') } />
               <RemoveCircleOutlineIcon color="error" fontSize="large" onClick={() =>  console.log('reject')} /> */}
                  
                 <Button variant="outlined" size="small" color="primary" className={classes.margin} onClick={() => console.log('accept') }>
                    accept
                </Button> 
                <Button variant="outlined" size="small" style={{color:'#000'}} className={classes.margin} onClick={() => console.log('reject') }>
                    Reject
                </Button> 
             </ListItem>
            );
           
        default: 
          return (
            <ListItem className={classes.item} style={{backgroundColor : bgColor} }  onClick={() => clickHandler(notification.type)}>
               <ListItemText primary={notification.message} />
             </ListItem>
          )
    }
}


export default NotificationItem;