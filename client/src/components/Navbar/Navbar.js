import React, { Fragment } from 'react';
import { Link , useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import NotificationList from '../Notifications/NotificationList';
import useStyles from './NavbarStyle';
import  { LOGOUT,SET_NOTIFICATION_COUNT, CLEAR_NOTIFICATIONS } from '../../redux/types';
import {
  AppBar,
  Toolbar,
  IconButton,
  Button,
  Typography,
  Badge,
  MenuItem,
  Menu,
  Avatar,
  Fade


} from '@material-ui/core';


import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
//import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

 function Navbar({auth, dispatch, notificationCount}) {
  const classes = useStyles();
  const history = useHistory();

  //const [notificationCount, setNotificationCount] = React.useState(0);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const [notificationsAnchorEl, setNotificationsAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const  isNotificationsOpened = Boolean(notificationsAnchorEl)

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleLogout = () => {
    dispatch({type:LOGOUT});
    dispatch({type:CLEAR_NOTIFICATIONS});
    
    history.push('/login');
  }

  const showNotifications = (event) => {
     dispatch({type:SET_NOTIFICATION_COUNT, payload:0});
     setNotificationsAnchorEl(event.currentTarget)
  }
  const closeNotifications = () => {
    
    setNotificationsAnchorEl(null)
 }

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem  component={Link} to={`/profile/${auth.user && auth.user.slug && auth.user.slug}`}  >Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
          component={Link} to={`/my-profile}`} 
        >
          <AccountCircle />
        </IconButton>
        <Link  to={`/profile/${auth.user && auth.user.slug && auth.user.slug}`} ><p>Profile</p></Link>
      </MenuItem>
      <MenuItem>
        { auth.isAuthenticated ? 
            <Button   aria-label="account of current user"
              aria-controls="primary-search-account-menu"
              aria-haspopup="true"
              color="inherit"
              component={Link} to="/login" ><p>login</p></Button>
           :
            <Button   
              aria-label="account of current user"
              aria-controls="primary-search-account-menu"
              aria-haspopup="true"
              color="inherit"
              onClick={()=> alert('test')} ><p>logout 1</p></Button>
          }
      </MenuItem>
        
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            Material-UI
          </Typography>
         
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
         {/*    <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen} 
              color="inherit"
            >
              <AccountCircle />
           
            </IconButton>
          
             */}
          
           <Button  color="secondary" component={Link} to="/"  style={{color:'white'}}>Home</Button>
          {!auth.isAuthenticated ?
            <Fragment>
              <Button  color="secondary" component={Link} to="/signup"  style={{color:'white'}} >Signup</Button>
              <Button  color="secondary" component={Link} to="/login"  style={{color:'white'}}>Login</Button>
           </Fragment>
          :
          <Fragment>
            <IconButton
                  edge="end"
                  aria-label="account of current user"
                  aria-controls={menuId}
                  aria-haspopup="true"
                /*   onClick={handleProfileMenuOpen} */
                  color="inherit"
                >
              { 
                auth && auth.user && auth.user.avatar ? 
                  <Avatar style={{height:'40px'}} alt="Rohn Jerremy" src={process.env.REACT_APP_ENDPOINT_BASE_URL+'/'+ auth.user.avatar} /> 
                  : <AccountCircle />
              }
            </IconButton>
             {auth.user && auth.user.firstName && 
              <Fragment>
                  <Button  color="secondary" component={Link}  to={`/my-profile`}   style={{color:'white'}}>
                    {auth.user.firstName}
                  </Button>
              </Fragment>
             }
          {/*   <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton> */}
            <IconButton aria-label="show 17 new notifications" color="inherit" onClick={showNotifications} >
              <Badge badgeContent={notificationCount} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <Menu
                id="fade-menu"
                anchorEl={notificationsAnchorEl}
                keepMounted
                open={isNotificationsOpened}
                onClose={closeNotifications}
                TransitionComponent={Fade}
                style={{top:40}}
              >
          
            
            <div>
                <NotificationList />
            </div>
         
             
              </Menu>
            <Button  color="secondary"  style={{color:'white'}} onClick={handleLogout}>Logout</Button>
          </Fragment> 
          
          }
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
            
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    notificationCount: state.notification.notificationCount
  }
}
export default connect(mapStateToProps)(Navbar)