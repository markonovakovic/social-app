import React, { Fragment } from 'react';
import './MyModal.css';

 const MyModal = ({open, title, close, children}) => {

     const handleClose = () =>{
        close(false)
     }
    return (
        <Fragment>
          

<div id="myModal" className="modal" style={open ? {display:'block',  opacity:"1"}: {display:'none'}}>

  <div className="modal-content">
    <div className="modal-header">
      <span className="close" onClick={handleClose} >&times;</span>
      <h3>{title}</h3>
    </div>
   
    {children}
    <div className="modal-footer">
      
    </div>
  </div>

</div>
            
</Fragment>
    )
}


export default MyModal;