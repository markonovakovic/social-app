import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import './Alert.css';

const Alert = ({alerts}) => 
    alerts !== null &&
    alerts.length > 0 &&
    <div className="alerts"> 
   { alerts.map(alert => {
        return  <div
            key={alert.id}
            id={alert.id}
            className={`alert alert-${alert.alertType}`}
        >
            <Typography style={{color:'white'}} >{alert.msg}</Typography>
        </div>
    })
    }
    </div>
    
 
Alert.propTypes = {
    alerts:PropTypes.array.isRequired
}

const mapStateProps = state => ({
    alerts: state.alerts
})

export default connect(mapStateProps)(Alert)