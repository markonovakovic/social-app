import React, {useEffect } from 'react';
import { connect }  from 'react-redux'
import { fetchFriendsByUser } from '../../../redux/actions/user';
import FriendCard from '../FriendCard/FriendCard';
import { CircularProgress }  from '@material-ui/core';


 function FriendList({friends, slug, fetchFriendsByUser, isOwner, marginB, loading}) {
    
    useEffect(() => {
        fetchFriendsByUser(slug);
       /*  return () => {

        } */
    },[slug]); //eslint-disable-line
    const friendList = () => {
        if(friends && !isOwner) return friends.filter(friend => friend.status ==='accepted');
        return friends
    }
        if(loading) {
          return  <CircularProgress />
        } else {
            return (
                <div>{
                    friendList().length !== 0  ? friendList().map((friend, key) => {
                        return <div key={key} style={{marginBottom:marginB}}> 
                                    <FriendCard friend={friend} marginB='10px' isOwner={isOwner}/>
                                </div>
                    })
                    : <div>No have friends</div>
                    }
                </div>
       )
    }
}

const mapStateToProps = state => ({
    friends: state.user.friends,
    loading: state.user.loadingFriends
})
export default connect(mapStateToProps,{fetchFriendsByUser})(FriendList)