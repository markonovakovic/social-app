import React, {useEffect} from 'react';
import  { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { fetchSuggestedFriends } from '../../../redux/actions/user';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import Typography from '@material-ui/core/Typography';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';  //eslint-disable-line
import StarBorderIcon from '@material-ui/icons/StarBorder'; //eslint-disable-line
import avatar from '../../../assets/images/avatar-def.png';
import SuggestedFriends from '../../../pages/SuggestedFriends/SuggestedFriends'; //eslint-disable-line

const useStyles = makeStyles(theme => ({

  gridList: {
    width:'33,3%',
   margin:0,
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  titleBar: {
    margin:0,
    height:'100%',
    background:
      'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
      'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
  listTile: {
    height: 450,  
     margin:0
  },
  icon: {
    color: 'white',
  },
}));


const SuggestedFriendsBox = ({friends_suggested, fetchSuggestedFriends}) => {
   
  const classes = useStyles();
  useEffect(() => {
    fetchSuggestedFriends();
},[]); //eslint-disable-line

const fullName = (firstName, lastName) => {
      return `${firstName} ${lastName}`;
}

  return (
    <div className={classes.root} >
      <Typography variant="h6" gutterBottom align="left">People You May Know</Typography>
      <Link to="/friends">
      <GridList cellHeight={150}  className={classes.gridList} >
        {friends_suggested && friends_suggested.map((friend, index) => (
          <GridListTile cols={1}  className={classes.listTile} key={friend.user._id}>
            <img src={friend.user.avatar ? process.env.REACT_APP_ENDPOINT_BASE_URL +'/'+ friend.user.avatar : avatar} alt={friend.user.firstName} style={{height:'98%'}} />
            <GridListTileBar
              title={fullName(friend.user.firstName, friend.user.lastName)}
              titlePosition="bottom"
             /*  actionIcon={
                <IconButton aria-label={`star ${friend.user.email}`} className={classes.icon}>
                  <StarBorderIcon />
                </IconButton>
              } */
              actionPosition="left"
              className={classes.titleBar}
            />
          </GridListTile>
        ))}
      </GridList>
      </Link>
    </div>
  );
}


const mapStateToProps = state => ({
    friends_suggested:state.user.friends_suggested
    
});


export default connect(mapStateToProps,{fetchSuggestedFriends})(SuggestedFriendsBox);