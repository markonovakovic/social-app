import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
    card: {
      display: 'flex',
      minHeight:'150px',
      width:'100%',
      [theme.breakpoints.down('xs')]: {
         display:'block',
         paddingBottom:'10px'
      }     
      
    },
    details: {
      display: 'flex',
      justifyContent:'space-between',
      alignItems:'center',
      width:'100%',
      [theme.breakpoints.down('sm')]: {
          flexDirection:'column',
          justifyContent:'space-around'
      }
    },
    content: {
        width:200,
        [theme.breakpoints.down('xs')]: {
            width:'100%'
         } 
    },
    cover: {
      width: 140,
      margin:'3px',
      minHeight:'140px',
      [theme.breakpoints.down('xs')]: {
        width:'100%'
        
     } 
    
    },
    linkCover:{
       textDecoration:'none',
       color:'#000',
       [theme.breakpoints.down('xs')]: {
        display:'block',
        width:'100%',
        padding:'0 19px'
     } 

    }
  }));
  

  export default useStyles;