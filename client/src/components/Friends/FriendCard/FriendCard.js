import React from 'react'
import { connect } from 'react-redux';
import {sendFriendshipRequest  } from '../../../redux/actions/user';
import { Link } from 'react-router-dom';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import avatar from '../../../assets/images/group.webp'
import Button from '@material-ui/core/Button';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CheckIcon from '@material-ui/icons/Check';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import useStyles from './FriendCardStyle';

 const FriendCard = ({status, date, _id, isOwner, friend, friend:{user} , sendFriendshipRequest}) => {
   /*   console.log(friend); */
    const classes =  useStyles();
    const friendshipStatusHandler = (action, slug) => {
       console.log(slug);
       sendFriendshipRequest(slug, action)
       if(action === 'add') {
       // sendFriendshipRequest(slug, action)
        console.log('add fr')
       } else if(action ==='withdraw') {
           console.log('wthdr fr')
     } else if(action ==='accept') {
            console.log('accept fr')
      } else if(action ==='reject') {
            console.log('reject fr')
       } else if(action === 'remove') {
        console.log('remove fr')
       }
    };

    const friendStatus =()=> {
        if(friend.status ==='requested') {
            return   <Button 
                        variant="outlined" 
                        style={{marginRight:'5px', 
                        fontSize:'0.6rem'}} 
                        size="small"
                        onClick={() => friendshipStatusHandler('withdraw', user.slug)}
                        ><GroupAddIcon fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}}/>
                        Friend request send
                    </Button>
        } else if(friend.status ==='accepted') {
            return   (
                <>
                   <Button 
                        variant="outlined"
                        style={{marginRight:'5px', fontSize:'0.6rem'}} 
                        size="small"
                        >
                            <CheckIcon fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}}/>
                            Friends
                    </Button>
                   <Button 
                       variant="outlined"
                       style={{marginRight:'10px', width:'100px' ,fontSize:'0.6rem'}} 
                       size="small"
                       onClick={() => friendshipStatusHandler('remove', user.slug)}
                       >
                           Remove
                    </Button> 
                </>
            )
        } else if(friend.status ==='pending') {
            return   (
                <>
                   <Button 
                        variant="outlined"
                        style={{marginRight:'5px', fontSize:'0.6rem'}} 
                        size="small"
                        onClick={() => friendshipStatusHandler('accept', user.slug)}
                        >
                            <PersonAddIcon fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}}/>
                            Accept Friend
                    </Button>
                   <Button 
                       variant="outlined"
                       style={{marginRight:'10px', width:'100px' ,fontSize:'0.6rem'}} 
                       size="small"
                       onClick={() => friendshipStatusHandler('reject', user.slug)}
                       >
                           <PersonAddDisabledIcon   fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}} />
                           Reject
                    </Button> 
                </>
            )
        } else {
            return  <Button
                        variant="outlined"
                        style={{marginRight:'5px', fontSize:'0.6rem'}} 
                        size="small"
                        onClick={() => friendshipStatusHandler('add', user.slug)}
                        >
                            <AddCircleOutlineIcon fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}}/>
                                Add Friends
                    </Button>
        }
      
    }; 
    return (
        <Card className={classes.card}>
        <Link to={`/profile/${user.slug}`}>
            <CardMedia
                className={classes.cover}
                image= {user.avatar ? process.env.REACT_APP_ENDPOINT_BASE_URL +'/'+ user.avatar : avatar}
                title={user.firstName}
            />
        </Link>
        <div className={classes.details}>
            <CardContent className={classes.content}>
                <Link to={`/profile/${user.slug}`} className={classes.linkCover} >
                    <Typography 
                        align="left"
                        component="h6"
                        variant="h6"
                        style={{letterSpacing:'0rem',fontSize:'1rem'}}>
                            {user.firstName}
                    </Typography>
                    <Typography align="left" variant="subtitle1" color="textSecondary">
                        Mac Miller
                    </Typography>
                </Link>
            </CardContent>
           { isOwner && <div>
           { friendStatus() }
             {/*  <Button variant="outlined" style={{marginRight:'5px', fontSize:'0.6rem'}} size="small"><AddCircleOutlineIcon fontSize="small" style={{fontSize:'1rem', marginRight:'5px'}}/>Add Friends</Button>
              <Button variant="outlined" style={{marginRight:'10px', width:'100px' ,fontSize:'0.6rem'}} size="small">Remove</Button> */}
            </div>
           } 
        </div>
    </Card>
    )
};

export default connect(null, {sendFriendshipRequest})(FriendCard);