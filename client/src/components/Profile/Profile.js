import React, { useState, useEffect, useRef } from 'react';
import { useParams,useHistory  } from "react-router-dom";
import moment from 'moment';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import  {fetchGroups,withdrawJoinGroupRequest, updateUser, fetchFriendsByUser }  from '../../redux/actions/user'
import avatar from '../../assets/images/profile.jpg'
import PostList from '../../components/Posts/PostList/PostList';
import GroupCard from '../../components/Groups/GroupCard/GroupCard';
import FriendList from '../../components/Friends/FriendsList/FriendList';
import SuggestedFriendsBox from '../../components/Friends/SuggestedFriendsBox/SuggestedFriendsBox';
import About from './About/About'
import CreatePost from '../../components/Posts/CreatePost/CreatePost';
import useStyles from './ProfileStyle';
import { 
    Grid,
    Paper,
    Typography,
    Divider,
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    Tabs, 
    Tab,
    Box
 } from '@material-ui/core/';

 import BackupIcon from '@material-ui/icons/Backup';
 import EditIcon from '@material-ui/icons/Edit';

 const Profile = ({fetchGroups, fetchFriendsByUser, user, groups, loading, friends, withdrawJoinGroupRequest,updateUser, isOwner}) => {
    let params = useParams();
    let history = useHistory();
    const classes = useStyles();
    const [tab, setTab] = useState(1);
    const img =  useRef(null);

    const search = window.location.search;

    useEffect(() => {
        
        const params = new URLSearchParams(search);
        setTab(Number(params.get('tab')));
       
    },[window.location.search]);
    useEffect(() =>{
        if(user && user.slug) {
        fetchGroups(user.slug);
        fetchFriendsByUser(user.slug);
        }
    },[user]) //eslint-disable-line

      useEffect(() =>{
          if(user && user.slug) {
            fetchGroups(user.slug);
           // fetchFriendsByUser(user.slug);
          }
     },[user]) //eslint-disable-line
    const handleEdit = (field) => {
           console.log(field);
    } 
    const handleChangeTab = (event, newValue) => {
        setTab(newValue);
        history.push(window.location.pathname + "?tab="+ String(newValue));
      };

    const withdrawJoinGroupRequestHandler =(groupId) => {
        if(user) {
            withdrawJoinGroupRequest(groupId, user._id)
        } else {
      /*     setAlert('You must be registred to send join request ') */
        }
        
    }
    const changeHandler = e => {
      console.log(img.current.files[0]);
      const data =  new FormData();
      data.append('avatar', img.current.files[0]);
      updateUser(data);

    }

    const uploadAvatrHandler =()=> {
        img.current.click();
    }

    return (
        <>
            <Paper>
            <Grid container style={{paddingTop:'20px'}}>
                <Grid item sm={4} xs={12}>
                <div className={classes.profileImg}>
                    
                    {isOwner && <BackupIcon className={classes.dropIcons} onClick={uploadAvatrHandler}/> }
                    <input type="file" name="avatar" ref={img}  style={{display:'none'}} onChange={changeHandler} />
                { user.avatar ?  
                        <img src={process.env.REACT_APP_ENDPOINT_BASE_URL+'/' + user.avatar || avatar} className={classes.large} alt="avatar"/>
                    : <img src={avatar} className={classes.large} alt="avatar"/>              
                } 
                </div>
                
                </Grid>
                <Grid item sm={8} xs={12}>
                    <div style={{padding:'10px'}}>
                        <Typography variant="h4" component="h4" align="left">
                            {user.firstName}  {user.lastName}
                        </Typography>
                
                        <Divider orientation="horizontal" />
                        <List component="nav" aria-label="mailbox folders">
                            <ListItem  className={classes.listItem}>
                            <div className={classes.listItemContent}>
                                <ListItemText secondary="Age:" className={classes.listItemTitle} />
                                <ListItemText primary={user.born ? moment().diff(user.born, 'years') : '-'}  style={{flex:'none', paddingLeft:'10px', width:'fit-content'}} /> 
                                {/*  <input type="text" onChange={handleChange} value="24"/> */}

                                </div>
                                <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}} >
                                    <EditIcon  className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('name')} />
                                </ListItemIcon>
                            </ListItem>
                            <Divider orientation="horizontal" />
                            <ListItem  className={classes.listItem}>
                            <div className={classes.listItemContent}>
                                <ListItemText secondary="Email:"   className={classes.listItemTitle} />
                                <ListItemText primary={user.email ? user.email : '-'} style={{flex:'none', paddingLeft:'10px',width:'fit-content'}} />
                                </div>
                                <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}}>
                                    <EditIcon className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('email')} />
                                </ListItemIcon>
                            </ListItem>
                            <Divider orientation="horizontal" />
                            <ListItem  className={classes.listItem}>
                            <div className={classes.listItemContent}>
                                <ListItemText secondary="Phone:"   className={classes.listItemTitle} />
                                <ListItemText primary={user.phone ? user.phone : '-'}  style={{flex:'none', paddingLeft:'10px', width:'fit-content'}} />
                                </div>
                                <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}}>
                                    <EditIcon className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('phone')} />
                                </ListItemIcon>
                            </ListItem>
                            <Divider orientation="horizontal" />
                            <Tabs
                                value={tab}
                                indicatorColor="primary"
                                textColor="primary"
                                onChange={handleChangeTab}
                                aria-label="disabled tabs example"
                                style={{display:'flex',justifyContent:'space-between'}}
                            >
                                <Tab label="Timeline" className={classes.tab} />
                                <Tab label="About" className={classes.tab} />
                                <Tab label="Groups" className={classes.tab}/>
                                <Tab label="Friends" className={classes.tab}/>
                            </Tabs>

                        </List>
                
                </div>
                </Grid>
            </Grid>
            </Paper>
            <Grid container spacing={2} pr={0}  style={{paddingRight:'0'}} >
                <Grid item sm={4} xs={12} >
                    <div style={{paddingTop:'16px'}}>
                       <SuggestedFriendsBox />
                    </div>
                  
                </Grid>
            
                <Grid item sm={8} xs={12}>
                    <TabPanel value={tab} index={0} >
                        <CreatePost />
                        <PostList user={user} />
                    </TabPanel> 
                    <TabPanel value={tab} index={1} >
                       <About user={user} />
                    </TabPanel>
                    <TabPanel value={tab} index={2}>
                        {  loading && !isOwner && <div>Loading...</div> }
                        {
                        groups.length !== 0 ? groups.map(group => {
                
                            return  <GroupCard key={group.group._id} group={group} marginB='10px' onCLick={withdrawJoinGroupRequestHandler} isStatusJoinEnabled={isOwner}/>
                        })
                        : 
                        <div>No have groups</div>
                        }
                    </TabPanel>
                   
                    <TabPanel value={tab} index={3} >
                        <FriendList  slug={user.slug} isOwner={isOwner}  marginB='10px' />
                    </TabPanel>
                </Grid>
            </Grid>
        </>
    )
    
}


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && <Box pt={2}>{children}</Box>}
      </Typography>
    );
  }

  Profile.propTypes =  {
    user:PropTypes.object.isRequired,
  }
  

  const mapStateToProps = state => ({
      groups: state.user.groups,
      loading: state.user.loading,
      friends: state.user.friends
     // auth: state.auth
  })

export default connect(mapStateToProps, {fetchGroups, withdrawJoinGroupRequest, updateUser, fetchFriendsByUser})(Profile);