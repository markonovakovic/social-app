import React, {Fragment} from 'react';
import moment from 'moment';
import {
 Paper,
 Grid,
 Typography,
 Divider,
} from  '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    table: {
   
    },
    tableTitle: {
        width:'100px',
        color:'#999696',
        fontWeight:'600',
        padding:'10px'
    },
    tableInfo: {
        padding:'10px 5px'
    }
  });

const About = ({user}) => {
    const classes = useStyles();
    return (
        <Paper>
            <Grid container>
                <Grid item sm={3}>
                    <Typography className={classes.tableTitle}  variant="body2">About me</Typography>
                </Grid>
                <Grid item sm={9}>
                    <div className={classes.tableInfo}>
                        <Typography align='left' variant="body2" gutterBottom> {user.bio && user.bio}</Typography>
                    </div>
                </Grid>
            </Grid>
            <Divider />
            <Grid container>
                <Grid item sm={3}>
                    <Typography className={classes.tableTitle}  variant="body2">Birthday</Typography>
                </Grid>
                <Grid item sm={9}>
                    <div className={classes.tableInfo}>
                        <Typography align='left' variant="body2" gutterBottom> {user.born && moment(user.born).format('DD.MMMM.YYYY.')}  { user.born && ( moment().diff(user.born, 'years')+ ' years')}</Typography>
                    </div>
                </Grid>
            </Grid>
            <Divider />
            <Grid container>
                <Grid item sm={3}>
                    <Typography className={classes.tableTitle}  variant="body2">Address</Typography>
                </Grid>
                <Grid item sm={9}>
                    <div className={classes.tableInfo}>
                        <Typography align='left' variant="body2" gutterBottom>{user.address && user.address}  {user.city && ', ' + user.city}</Typography>
                    </div>
                </Grid>
            </Grid>
            <Divider />
            <Grid container>
                <Grid item sm={3}>
                    <Typography className={classes.tableTitle}  variant="body2"> My hobies</Typography>
                </Grid>
                <Grid item sm={9}>
                    <div className={classes.tableInfo}>
                        <Typography align='left' variant="body2" gutterBottom>
                        {user.hobies && user.hobies.map((hobby, index) => {
                            if(hobby === '') return
                            return <Fragment key={index}>
                                    <span>*{hobby}</span><br />
                                </Fragment>
                        })}
                        </Typography>
                    </div>
                </Grid>
            </Grid>
            <Divider />
        </Paper>
    )
}


export default About;