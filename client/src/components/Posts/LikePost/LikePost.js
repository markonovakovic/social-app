import React from 'react'
import { connect } from 'react-redux';
import  {  likePostToggle } from '../../../redux/actions/post';
import {
     makeStyles,
     Tooltip ,
     IconButton
    } from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';

const useStyles =  makeStyles({
    heartIcon: {
        padding:'3px',
        "&:hover": {
          backgroundColor: 'rgb(7, 177, 77, 0.42)'
        }
      },
      customWidth :{
          maxWidth:'85px'
      }
});


const LikePost = ({auth, likes, postId, onClick, likePostToggle}) => {
    const classes =  useStyles();
    const likeToolTipe = (likes) => {

       return  likes ?  likes.map(like => {
          return `* ${like.firstName} ${like.lastName} \n`;
        }).join('\n') 
        : [];
      }
    const  postIsLiked = () => {
          const index = likes.findIndex(like => like._id === auth._id);
          return index !== -1 ? '#df550f' : 'rgba(63, 61, 61, 0.54)';
    }
    return (
        <>
         <Tooltip title={likeToolTipe(likes)} classes={{ tooltip: classes.customWidth }} interactive>
            <IconButton aria-label="add to favorites" className={classes.heartIcon} onClick={()=> likePostToggle(postId, auth.user) } >
            <FavoriteIcon style={{color:postIsLiked()}} />
            </IconButton>
        </Tooltip>
            {likes.length}
        </>
    )
}

const mapStateToProps = state => ({
    auth: state.auth,

})
export default connect(mapStateToProps, { likePostToggle})(LikePost);