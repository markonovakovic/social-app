import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Grid,
  TextField, 
  FormControl,
  Input,
  InputLabel,
  Avatar,
  Chip,
  Button 

  } from '@material-ui/core';

  import DoneIcon from '@material-ui/icons/Done';
  import MyModal  from '../../UI/modal/MyModal';

  const useStyles = makeStyles(theme => ({
    root: {
      '& > *': {
       /*  margin: theme.spacing(1), */
        width: '100%',
      },
    },
  }));


const CreatePost = () => {
    const classes = useStyles();
    const [isOpened, setIsOpened ] = useState(false);
    const handleOpen = () => {
        setIsOpened(true);
    }

    const handleClose = (event) =>{
        setIsOpened(event)
    }
    return (
        <div>
            <FormControl style={{width:'100%', maxWdith:'500px', marginBottom:'30px'}}>
                <InputLabel htmlFor="component-simple">New Post</InputLabel>
                <Input id="component-simple"  onClick={handleOpen} style={{width:'100%'}} />
             </FormControl>

            <MyModal 
                 open={isOpened} 
                 title="Create Post"
                 close={handleClose}
                 > 
                <div className="modal-body">
                <form className={classes.root} noValidate autoComplete="off">
                 
                        <TextField
                          id="standard-textarea"
                          label="What's on your mind?"
                          placeholder="..."
                          multiline
                          style={{width:'100%'}}
                          rows="6"
                        />
                  
                  <Grid container>
                    <Grid item sm={12}>
                      <div style={{marginTop:'10px', marginBottom:'10px'}}>
                      <Chip
                          size="small"
                          avatar={<Avatar>M</Avatar>}
                          label="Images"
                          clickable
                          color="primary"
                        
                          deleteIcon={<DoneIcon />}
                          variant="outlined"
                        />
                         <Chip
                          size="small"
                          avatar={<Avatar>M</Avatar>}
                          label="Filing Activity"
                          clickable
                          color="secondary"
                        
                          deleteIcon={<DoneIcon />}
                          variant="outlined"
                        />
                         <Chip
                          size="small"
                          avatar={<Avatar>M</Avatar>}
                          label="Video"
                          clickable
                          color="primary"
                        
                          deleteIcon={<DoneIcon />}
                          variant="outlined"
                        />
                      

                      </div>
                    
                    </Grid>
                  </Grid>
                    <Button variant="contained" color="secondary" fullWidth>
                      Send
                    </Button>
                </form>
                </div>
           </MyModal>
        </div>
    )
}


export default CreatePost;