import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import PostCard from '../PostCard/PostCard';
import PropTypes from 'prop-types';
import {
    CircularProgress 
} from '@material-ui/core';
import { getPostsByUser, clearPosts } from '../../../redux/actions/post';
import {
    Paper,
    Fade 
} from '@material-ui/core';

 const PostList = ({
    getPostsByUser,
    posts,
    pagination,
    errors,
    loading,
    match,
    clearPosts,
    user
 }) => {



     useEffect(() =>{
        getPostsByUser(user.slug);
        return (() => {
          clearPosts();
        }) 
     },[user.slug]); //eslint-disable-line  

     const postList =  posts && posts.length !== null && posts.map(post => {
            return  <Paper square key={post._id} style={{marginBottom:'7px'}}>
                       <PostCard {...post} />
                    </Paper> 
     });

    let output = (
        <Fade
        in={loading}
      >
        <CircularProgress />
      </Fade>
    );
  
     if( posts.length !==0 && !loading) {
      output =  (
            <div>
              { postList }
             </div> 
         )
     } else if(loading) {
          output = (
            <Fade
            in={loading}
          >
            <CircularProgress />
          </Fade>
        );
     } else {
          output = <div> there is a no posts</div>
     }
     
    return (
     <Fragment>
        {output} 
    </Fragment>
    )
};

PostList.propTypes = {
   posts: PropTypes.array.isRequired,
 /*  pagination:PropTypes.object.isRequired, */ 
  getPostsByUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
   posts: state.post.posts,
   pagination: state.post.pagination,
   loading:state.post.loading,
   errors:state.post.errors
});


export default connect(mapStateToProps,{
    getPostsByUser,
    clearPosts
})(PostList);
