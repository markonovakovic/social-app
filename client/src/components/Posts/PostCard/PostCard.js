import React from 'react';
import useStyles from './style';
import Moment from 'moment';
import LikePost from '../LikePost/LikePost';

import {
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    Avatar,
    IconButton,
    Typography,

} from '@material-ui/core';


import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';



export default function RecipeReviewCard(post) {
  const classes = useStyles();
  // eslint-disable-next-line
  const {
    _id,
    createdAt,
    likes, //eslint-disable-line
    status, //eslint-disable-line
    user,
    content,
    comments, //eslint-disable-line
    thumbnail

  } = post; 

  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  
  const getFullName = (firstName, lastName ) => {
         return firstName +'  '+ lastName;
  }
  const nameInitial = (name) => {
    return  name.charAt(0).toUpperCase();
  }

  const isContentTooShort = (content,characters = 257) => {
    return  content.length < characters ? true : false;
  }
  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar} src={process.env.REACT_APP_ENDPOINT_BASE_URL + '/'+ user.avatar} >
           {nameInitial(user.firstName)}
           
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={getFullName(user.firstName, user.lastName)}
        subheader={Moment(createdAt).format("DD.MM.YYYY, h:mm")}
        align="left"
      />
      <CardContent className={classes.cardContent} style={expanded ?{ height:'max-content'} :{height:'60px'}}>
        <Typography variant="body2" color="textSecondary" component="p" aling="left">
         {content}
        </Typography>
        { !isContentTooShort(content,255) &&
        <Typography 
           variant="body2" 
           component="p"
           aling="left"
           onClick={handleExpandClick}
           style={{position:'absolute',bottom:0,right:'10px', background:'#fff'}}
           >{ !expanded ?'...Read more': 'Read less'}</Typography>
          }
         </CardContent>
      { thumbnail &&
          <CardMedia
          className={classes.media}
          image={thumbnail}
          title="Paella dish"
        />
      }
      <CardActions disableSpacing>
        <LikePost likes={likes} postId={_id} />

        <IconButton aria-label="share" style={{marginLeft:'10px',padding:'3px'}}>
          <ShareIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}
