import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import GroupIcon from '@material-ui/icons/Group';
import AddIcon from '@material-ui/icons/Add';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import CreateGroup from '../Groups/CreateGroup/CreateGroup';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 260,
   
  },
  nested: {
  
  },
  link:{
      textDecoration:'none',
      color:'#000'
  },
  icon:{
   width:'0.8em'
  },
  navbarIcon:{
    minWidth:'30px'
  }
}));

export default function Sidebar(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [openNewGroup, setOpenNewGroup] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };
  const openNewGroupModal = ()=>{
    setOpenNewGroup(true)
  }

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Nested List Items
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem button>
        <ListItemIcon className={classes.navbarIcon} >
          <SendIcon className={classes.icon}/>
        </ListItemIcon>
        <ListItemText primary="Sent mail" />
      </ListItem>
      <Link to="/groups" className={classes.link}>
      <ListItem button >
          
        <ListItemIcon className={classes.navbarIcon}>
         <InboxIcon className={classes.icon} />
        </ListItemIcon>
        <ListItemText primary="Groups" />
        
      </ListItem>
      </Link>
      <Link to="/friends" className={classes.link}>
      <ListItem button to="/friends" >
        <ListItemIcon className={classes.navbarIcon}>
          <EmojiPeopleIcon className={classes.icon} />
        </ListItemIcon>
        <ListItemText primary="Find Friends" />
      </ListItem>
      </Link>
      <ListItem button onClick={handleClick}>
        <ListItemIcon className={classes.navbarIcon}>
        <GroupIcon  className={classes.icon}/>
         
        </ListItemIcon>
        <ListItemText primary="Groups" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <CreateGroup open={openNewGroup} handleClose={()=> setOpenNewGroup(false)} />
        <ListItem button className={classes.nested} onClick={openNewGroupModal}>
            <ListItemIcon className={classes.navbarIcon}>
              <AddIcon className={classes.icon} />
            </ListItemIcon>
            <ListItemText primary="Creat" />
          </ListItem>
          <Link to="/groups" className={classes.link}>
            <ListItem button className={classes.nested}>
              <ListItemIcon className={classes.navbarIcon}>
                <StarBorder className={classes.icon} />
              </ListItemIcon>
              <ListItemText primary="My Groups" />
            </ListItem>
          </Link>
        </List>
      </Collapse>
    </List>
  );
}