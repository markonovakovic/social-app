import React from 'react';
import  { connect } from 'react-redux';
import GroupCard from '../GroupCard/GroupCard';
import { 
  Box, 
  Typography
} from '@material-ui/core'

 const GroupList = ({tab,value, index, other, groups, loading}) => {
    return (
        <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
     
     
   
      {value === index && <Box pt={2}>
      
      {  loading && <div>Loading...</div> }
        {
           groups.length !== 0 ? groups.map(group => {

            return  <GroupCard key={group.group._id} group={group} marginB='10px' />
           })
           : 
           <div>No have groups</div>
        }
      }</Box>}
      </Typography>
    )
}

/* const mapStateToProps = state => {
    group:state.user.groups
}; */

export default GroupList;