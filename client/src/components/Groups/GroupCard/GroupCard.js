import React from 'react';
import { Link } from 'react-router-dom';
import {
    Card,
    CardMedia,
    CardContent,
    Typography,
    Button
} from '@material-ui/core';
import useStyles from './style';
import avatar from '../../../assets/images/group.webp'

 const GroupCard = ({group,group:{name}, marginB, onCLick,  style, isStatusJoinEnabled}) => {

    const classes =  useStyles();
    const thumbnail = group.group.imageUrl ? group.group.imageUrl : avatar
    return (
             <Card className={classes.card} style={{marginBottom:marginB}}>
                      <Link to={`/groups/${group.group._id}`} className={classes.linkCover}>
                        <CardMedia
                            className={classes.cover}
                            image={thumbnail}
                            title={group.group.name}
                        />
                    </Link>

                 
                    <div style={style} className={classes.details}>
                        <CardContent className={classes.content}>
                            <Link to={`/groups/${group.group._id}`} className={classes.linkCover} >
                                <Typography 
                                    align="left"
                                    component="h6"
                                    variant="h6"
                                    style={{letterSpacing:'0rem',fontSize:'1rem'}}>
                                          {group.group.name}
                                </Typography>
                                <Typography align="left" variant="subtitle1" color="textSecondary">
                                  {group.group.description}
                                </Typography>
                            </Link>
                        </CardContent>
                       {
                       isStatusJoinEnabled ?

                            <Button
                                variant="outlined"
                                style={{marginRight:'10px'}}
                                onClick={() => onCLick(group.group._id)} 
                                size="small">
                                    { group.status === 'requested'? 'Requested':'Join'}
                            </Button>
                        : null  
                      }
                        
                    </div>
                </Card>
      
    )
}


GroupCard.defaultProps = {
    isStatusJoinEnabled: true
}

export default GroupCard;