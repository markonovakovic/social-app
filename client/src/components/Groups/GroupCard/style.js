
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme =>({
    card: {
      display: 'flex',
      minHeight:'150px',
      
 
    },
    details: {
      display: 'flex',
      justifyContent:'space-between',
      alignItems:'center',
      width:'100%'
    },
    content: {

    },
    cover: {
      width: 140,
      margin:'3px',
      minHeight:'140px'
    
    },
    linkCover:{
       textDecoration:'none',
       color:'#000'
    }
}));

export default useStyles;