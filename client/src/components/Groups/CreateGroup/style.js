const styles = theme => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
    modal: {
        minWidth:'600px',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(3),
          width: 'auto',
        },
    },
    dropContainer:{
      position:'relative',
      height:'120px'
    },
    uploadImage:{
      width:'90px',
      height:'90px',
      display:'inline-block',
      position:'absolute'  
  },
    photoIcon:{
      fontSize:'2rem',
      position:'absolute'
    }
  });

  export default styles;