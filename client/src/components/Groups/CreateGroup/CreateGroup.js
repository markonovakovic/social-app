import React, {useRef} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import styles from './style';
/* import groupImage from '../../assets/images/group.webp'; */


const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(props) {
/*   const [open, setOpen] = React.useState(false); */
  const [name, setName] = React.useState("");
  const image = useRef(null);

/*   const handleClickOpen = () => {
    setOpen(true);
  }; */
  const handleClose = () => {
 /*    setOpen(false); */
    props.handleClose()
  };

  const handleChange = (e) => {
    console.log(image);
   console.log(e.target.value);
   if(e.target.name === 'name') setName(e.target.value);

  }

  const handleSubmit = (e) =>{
   e.preventDefault();
   console.log('submitovano')
  }
   const handleInputFile = () => {

       console.log('test', image)
         image.current.click();
   }

  return (
    <div>

      <Dialog onClose={props.handleClose} aria-labelledby="customized-dialog-title" fullWidth maxWidth="sm" open={props.open}>
       <form  noValidate autoComplete="off" onSubmit={handleSubmit}>
        <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
          Create New Group
        </DialogTitle>
        <DialogContent dividers className={styles.modal}>
        

    <TextField
          id="outlined-password-input"
          label="Name your group"
          name="name"
          value={name}
          autoFocus
          variant="outlined"  fullWidth 
          className={styles.inputField}
          />
          <div onClick={handleInputFile} style={{height:'40px', margin:'10px 0 10px 0', display:'flex', justifyContent:'center', alignItems:'center', background:'rgb(220, 216, 216)', padding:'5px'}} className={styles.dropContainer}>
              <input type="file" name="image" style={{display:'none'}}  ref={image}   onChange={handleChange}/>
         {/*  <CardMedia
            className={styles.uploadImage}
            image={groupImage}
            style={{width:'90px', height:'90px', position:'absolute',display:'inline-block'}}  />*/}
               
           
                <PhotoCameraIcon      style={{ color: '#009fff', fontSize:'2.5rem', left:'15px', top:'15px', }} className={styles.photoIcon}/>
          </div>
          </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} variant="contained" color="primary">
            Create
          </Button>
        </DialogActions>

          
    </form>
       
      </Dialog>
    </div>
  );
}
