import {makeStyles} from '@material-ui/core/';

 const useStyles = makeStyles(theme => ({
    large:{
        width:'100%'
    },
    profileImg:{
       position:'relative',
       maxWidth:'200px',
       margin:'auto',
    },
    dropIcons:{
        position:'absolute',
        bottom:0,
        width:'100%',
        fontSize:'50px',
        background:'rgba(255,255,255, 0.5)'
    },
    listItem: {
         paddingTop:0,
         paddingBottom:0,
         justifyContent:'space-between',
        '&:hover':{
           background:'none'
        },
      '&:hover $editIcon': {
          opacity:1,
          visibility:'visible',
          path: {
              fill: 'red'
          }
      }
     },
      editIcon:{
          visibility:'hidden',
          opacity:0,
          color:'purple',
          fontSize: '0.9rem',
      
  
      },
      listItemTitle:{
          minWidth:'70px'
      
      },
      listItemContent:{
        display:'flex',
        [theme.breakpoints.down('xs')]: {
         display:'block'
        },
      },
      groupItem:{
          marginBottom:'5px',
          paddingBottom:'5px'
      }
    }));

    export default  useStyles;