import React, { useState, useEffect, useRef } from 'react'
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import  { getUser, fetchGroups,withdrawJoinGroupRequest, updateUser }  from '../../redux/actions/user'
import Layout from '../../components/Layout';
import avatar from '../../assets/images/profile.jpg'
import PostList from '../../components/Posts/PostList/PostList';
import GroupCard from '../../components/Groups/GroupCard/GroupCard';
import CreatePost from '../../components/Posts/CreatePost/CreatePost';
import ProgresBar from '../../components/UI/ProgresBar/ProgresBar';

import useStyles from './ProfileStyle';
import { 
    Grid,
    Paper,
    Typography,
    Divider,
    List,
    ListItem,
    ListItemText,
    ListItemIcon,
    Tabs, 
    Tab,
    Box
 } from '@material-ui/core/';

 import BackupIcon from '@material-ui/icons/Backup';
 import EditIcon from '@material-ui/icons/Edit';

 const Profile = ({getUser, fetchGroups, user, groups, loading, auth, withdrawJoinGroupRequest,updateUser}) => {
    const classes = useStyles();
    const [tab, setTab] = useState(0);
    const img =  useRef(null);

    const urlParams = useParams();
    useEffect(() =>{
        getUser(urlParams.slug);
     },[urlParams.slug]) //eslint-disable-line

     useEffect(() =>{
        fetchGroups(urlParams.slug);
     },[urlParams.slug]) //eslint-disable-line
    const handleEdit = (field) => {
           console.log(field);
    }
    const handleChangeTab = (event, newValue) => {
        setTab(newValue);
      };

      const withdrawJoinGroupRequestHandler =(groupId) => {
        if(auth.isAuthenticated) {
            withdrawJoinGroupRequest(groupId, auth.user._id)
        } else {
      /*     setAlert('You must be registred to send join request ') */
        }
        
    }
    const changeHandler = e => {
      console.log(img.current.files[0]);
      const data =  new FormData();
      data.append('avatar', img.current.files[0]);
      updateUser(data);

    }

    const uploadAvatrHandler =()=> {
        img.current.click()
        console.log(img)
    }
    const isStatusJoinEnabled = () => {
        if(auth.user.slug === user.slug && auth.isAuthenticated) {
            return true
        }
        return false;
    }
    const isOwner = () => {
        if(auth.user && auth.isAuthenticated && auth.user._id === user._id) return true;
        return false;
    }
    const avatarImg = () => {
        return process.env.REACT_APP_ENDPOINT_BASE_URL+'/'+ user.avatar;
    }
    if(loading) {
         return <ProgresBar />
    } else  {
        return (
            <Layout injectFirst>
                <Paper>
                <Grid container style={{paddingTop:'20px'}}>
                    <Grid item sm={4} xs={12}>
                    <div className={classes.profileImg}>
                        
                        {isOwner() && <BackupIcon className={classes.dropIcons} onClick={uploadAvatrHandler}/> }
                        <input type="file" name="avatar" ref={img}  style={{display:'none'}} onChange={changeHandler} />
                    { user.avatar ?  
                         <img src={'http://localhost:5000/'+ user.avatar || avatar} className={classes.large} alt="avatar"/>
                        : <img src={avatar} className={classes.large} alt="avatar"/>              
                   } 
                    </div>
                    
                    </Grid>
                    <Grid item sm={8} xs={12}>
                        <div style={{padding:'10px'}}>
                            <Typography variant="h4" component="h4" align="left">
                                {user.firstName}  {user.lastName}
                            </Typography>
                    
                            <Divider orientation="horizontal" />
                            <List component="nav" aria-label="mailbox folders">
                                <ListItem  className={classes.listItem}>
                                <div className={classes.listItemContent}>
                                    <ListItemText secondary="Age:" className={classes.listItemTitle} />
                                    <ListItemText primary={user.born ? user.born: '-'}  style={{flex:'none', paddingLeft:'10px', width:'fit-content'}} /> 
                                    {/*  <input type="text" onChange={handleChange} value="24"/> */}

                                    </div>
                                    <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}} >
                                        <EditIcon  className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('name')} />
                                    </ListItemIcon>
                                </ListItem>
                                <Divider orientation="horizontal" />
                                <ListItem  className={classes.listItem}>
                                <div className={classes.listItemContent}>
                                    <ListItemText secondary="Email:"   className={classes.listItemTitle} />
                                    <ListItemText primary={user.email ? user.email : '-'} style={{flex:'none', paddingLeft:'10px',width:'fit-content'}} />
                                    </div>
                                    <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}}>
                                        <EditIcon className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('email')} />
                                    </ListItemIcon>
                                </ListItem>
                                <Divider orientation="horizontal" />
                                <ListItem  className={classes.listItem}>
                                <div className={classes.listItemContent}>
                                    <ListItemText secondary="Phone:"   className={classes.listItemTitle} />
                                    <ListItemText primary={user.phone ? user.phone : '-'}  style={{flex:'none', paddingLeft:'10px', width:'fit-content'}} />
                                    </div>
                                    <ListItemIcon className="info-item__listIcon" style={{minWidth:'0'}}>
                                        <EditIcon className={classes.editIcon} style={{fontSize:'0.9rem'}} onClick={() => handleEdit('phone')} />
                                    </ListItemIcon>
                                </ListItem>
                                <Divider orientation="horizontal" />
                                <Tabs
                                    value={tab}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    onChange={handleChangeTab}
                                    aria-label="disabled tabs example"
                                >
                                    <Tab label="Timeline" />
                                    <Tab label="Groups" />
                                    <Tab label="Friends" />
                                </Tabs>

                            </List>
                    
                    </div>
                    </Grid>
                </Grid>
                </Paper>
            
                <Grid container spacing={2} pr={0}  style={{paddingRight:'0'}} >
                    <Grid item sm={4} xs={12}>
                    
                    </Grid>
                
                    <Grid item sm={8} xs={12}>
                    <TabPanel value={tab} index={0} >
                        <CreatePost />
                        <PostList />
                    </TabPanel> 
                    <TabPanel value={tab} index={1}>
                    {  loading && <div>Loading...</div> }
                    {
                       groups.length !== 0 ? groups.map(group => {
            
                        return  <GroupCard key={group.group._id} group={group} marginB='10px' onCLick={withdrawJoinGroupRequestHandler} isStatusJoinEnabled={isStatusJoinEnabled()}/>
                       })
                       : 
                       <div>No have groups</div>
                    }
                       {/*  <GroupCard group={{name:'Grupa baja'}} marginB='10px' />
                
                        <GroupCard group={{name:'Grupa baja'}}  marginB='10px'  />
                
                        <GroupCard group={{name:'Grupa baja'}}  marginB='10px' />
                    
                        <GroupCard group={{name:'Grupa baja'}}  marginB='10px' />
                    
                        <GroupCard group={{name:'Grupa baja'}}  marginB='10px' />
                    
                        <GroupCard group={{name:'Grupa baja'}}  marginB='10px' /> */}
                    
                    </TabPanel>
                    <TabPanel value={tab} index={2} >
                    Item Three
                    </TabPanel>
                    </Grid>
                </Grid>
            </Layout>
        )
    }
}


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && <Box pt={2}>{children}</Box>}
      </Typography>
    );
  }

  Profile.propTypes =  {
    getUser: PropTypes.func.isRequired
  }
  

  const mapStateToProps = state => ({
      user: state.user.user,
      groups: state.user.groups,
      loading: state.user.loading,
      auth: state.auth
  })

export default connect(mapStateToProps, {getUser, fetchGroups, withdrawJoinGroupRequest, updateUser})(Profile);