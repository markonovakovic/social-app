import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import  { getUser ,withdrawJoinGroupRequest, updateUser }  from '../../redux/actions/user'
import Layout from '../../components/Layout';
import ProgresBar from '../../components/UI/ProgresBar/ProgresBar';
import Profile from '../../components/Profile/Profile';


 const MyProfile = ({getUser, user,  loading}) => {
   
 
    const urlParams = useParams();

    
    useEffect(() =>{
        getUser(urlParams.slug);
     },[urlParams.slug]) //eslint-disable-line

    if(loading) {
         return <ProgresBar />
    } else  {
        return (
            <Layout injectFirst>
                <Profile user={user} isOwner={false} />
            </Layout>
        )
    }
}


  Profile.propTypes =  {
    getUser: PropTypes.func
  }
   

  const mapStateToProps = state => ({
      user:state.user.user,
      loading: state.user.loading,
      loadingAuth: state.auth.loading,
      auth: state.auth
  })

export default connect(mapStateToProps, {getUser, withdrawJoinGroupRequest, updateUser})(MyProfile);