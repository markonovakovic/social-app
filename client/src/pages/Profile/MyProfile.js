import React, {useEffect} from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import  { getUser, fetchGroups,withdrawJoinGroupRequest, updateUser }  from '../../redux/actions/user'
import Layout from '../../components/Layout';
import ProgresBar from '../../components/UI/ProgresBar/ProgresBar';
import Profile from '../../components/Profile/Profile';


 const MyProfile = ({getUser, loading, auth}) => {
    useEffect(() =>{
        if(auth.user && auth.user.slug) {
            getUser(auth.user.slug);
        }
     },[auth.user]) //eslint-disable-line

    if(loading) {
         return <ProgresBar />
    } else  {
        return (
            <Layout injectFirst>
                <Profile user={auth.user} isOwner={true} isAuthenticated={auth.isAuthenticated} />
            </Layout>
        )
    }
}

  Profile.propTypes =  {
    getUser:PropTypes.func,
    auth: PropTypes.object
  }
   
  const mapStateToProps = state => ({
     /*  loading: state.user.loading, */
      loading: state.auth.loading,
      auth: state.auth
  })

export default connect(mapStateToProps, {getUser, fetchGroups, withdrawJoinGroupRequest, updateUser})(MyProfile);