import React, { useEffect} from 'react';
import { connect } from 'react-redux';
import { fetchSuggestedFriends } from '../../redux/actions/user';
import { Grid } from '@material-ui/core/';
import Sidebar from '../../components/Sidebar/Sidebar';
import CreatePost from '../../components/Posts/CreatePost/CreatePost';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';




import Layout from '../../components/Layout';
import FriendCard from '../../components/Friends/FriendCard/FriendCard';

const SuggestedFriends = ({friends_suggested, fetchSuggestedFriends}) => {

   useEffect(() => {
       fetchSuggestedFriends();
   },[]); //eslint-disable-line
    const cards = friends_suggested && friends_suggested.map((friend,key)=>{
        return <Grid item sm={12} md={12}  key={key} style={{width:'100%'}}> 
                   <FriendCard friend={friend} marginB='10px' isOwner={true}/>
                    <Divider />
                </Grid>
    })
  return (
      <Layout>
        <Grid container spacing={3} >
            <Grid item sm={2} xs={12}>
                <Sidebar />
            </Grid>
            <Grid item  sm={8} xs={12}>
                <CreatePost />
                <Paper>
                    <Grid container spacing={1} >
                    {cards}
                    </Grid>
                </Paper>
                
            </Grid>
            <Grid item sm={2} xs={12}>
                <Sidebar />
            </Grid>
        </Grid>
     </Layout>
  );
}

const mapStateToProps = state => ({
    friends_suggested:state.user.friends_suggested
    
});

export default connect(mapStateToProps, { fetchSuggestedFriends })(SuggestedFriends);
