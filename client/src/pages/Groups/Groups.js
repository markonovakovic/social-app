import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import  { sendJoinRequest, fetchSuggestedGroup } from '../../redux/actions/group';
import  { setAlert } from '../../redux/actions/alert';
import GroupCard from '../../components/Groups/GroupCard/GroupCard';
import { Grid } from '@material-ui/core/';
import Layout from '../../components/Layout';
import Sidebar from '../../components/Sidebar/Sidebar';
import CreatePost from '../../components/Posts/CreatePost/CreatePost';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';

function Groups({groups,fetchSuggestedGroup, sendJoinRequest, auth}) {
   // const classes = useStyles();

    useEffect(() => {
        fetchSuggestedGroup();
    },[]);// eslint-disable-line
    
  const joinGroupRequestHandler =(groupId) => {
      if(auth.isAuthenticated) {
        sendJoinRequest(groupId, auth.user._id)
      } else {
        setAlert('You must be registred to send join request ')
      }
      
  }
    const cards = groups.map(group=>{
         return <Grid item sm={6} md={6}  key={group.group._id}> 
                  <GroupCard group={group} onCLick={joinGroupRequestHandler} style={{flexDirection:'column',padding: '10px 0'}} />
                <Divider />
            </Grid>
 })
  return (
      <Layout>
        <Grid container spacing={3} >
            <Grid item sm={2} xs={12}>
                <Sidebar />
            </Grid>
            <Grid item  sm={10} xs={12}>
                <CreatePost />
                <Paper>
                    <Grid container spacing={1} >
                    {cards}
                    </Grid>
                </Paper>
                
            </Grid>
        </Grid>
     </Layout>
  );
}

const mapStateToProps = state => ({
    groups: state.group.suggested,
    auth: state.auth
})
export default  connect(mapStateToProps, {sendJoinRequest, fetchSuggestedGroup, setAlert})(Groups);