import React,{useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { connect }  from 'react-redux';
import { register } from '../redux/actions/auth';
import { setAlert } from '../redux/actions/alert';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';

import {
    Grid,
    Button,
    Typography,
    TextField,
    Paper,
    Divider

} from '@material-ui/core';
import CheckRoundedIcon from '@material-ui/icons/CheckRounded';


const styles = {
    pageTitle:{
      marginBottom:'30px'
    },
    form:{
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        maxWidth:'400px'
        
    },
    textField:{
        marginBottom:'7px',
        width:'100%'
    },
    inputSection:{
        marginBottom:'7px'
    },
    error:{
        margin:'3px',
        textAlign:'left',
        color:'red',
        fontSize:'12px',
        paddingLeft:'15px'
    }

}

const isValid = (errors) => {
   return Object.keys(errors).length > 0 ? false : true;
} 


const  Signup = ({classes, register, setAlert, success}) => {
    const [data, setData] = useState({
        firstName:'',
        lastName:'',
        email:'',
        password:'',
        password2:''
    });
    const [errors, setErrors] = useState({
        firstName:'',
        lastName:'',
        email:'',
        password:'',
        password2:''
    });
    const [isSuccessfulRegistred, setIsSuccessfulRegistred ] = useState(false)
   useEffect(() =>{  
     if(success) {
        setIsSuccessfulRegistred(true)
     }
   },[success])
    const handleSubmit = async() =>{
         if(!isValid(errors)) return;
         register(data);
       
        setData({
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            password2:''
        })
    }
    const handleChange = (e) => {
       setData({...data, [e.target.name]:e.target.value});
       validation(e.target.name, e.target.value);
    }
    const validation = (field, value) => {
         switch(field) {
            case 'firstName':
                value === ''? 
                    setErrors({...errors, firstName: 'field is required'})
                    : delete errors.firstName;
                value.length < 3 ?
                    setErrors({...errors, firstName: 'min 3 characters'})
                    :delete errors.firstName;
                break;
            case 'lastName':
                value === ''? 
                    setErrors({...errors, lastName: 'field is required'})
                    : delete errors.lastName;
                value.length < 3 ?
                    setErrors({...errors, lastName: 'min 3 characters'})
                    :delete errors.lastName;
                break;
            case 'email':
                const regex =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                //var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@\S+\.\S{2,3}$/;
                const res =  regex.test(String(value).toLowerCase());
                !res ?  setErrors({...errors, email:'Mail is not valid'})
                : delete errors.email;
                break;
            case 'password':
                value === ''? 
                    setErrors({...errors, password: 'field is required'})
                    : delete errors.password;
                value.length < 3 ?
                    setErrors({...errors, password: 'min 3 characters'})
                    :delete errors.password;
                break;
            
            case 'password2':
                value !== data.password ?
                setErrors({...errors, password2: 'password doesn\'t match'})
                : delete errors.password2;
                break;
            default:
                
        }
            
    }
    const verificationMsg = () => {
        return (
            <Paper>
                <div style={{ padding:'10px', height:'150px'}}>
                    <div style={{display:'flex'}}>
                        <CheckRoundedIcon />
                        <Typography align="left">A verification link has been send to your email account</Typography> 
                    </div>
                    <Divider orientation="horizontal" />
                    <Typography 
                        aling="left"
                        color="textSecondary"
                        component="p"
                        variant="subtitle2"
                        style={{paddingLeft:'10px', lineHeight:'1.1rem'}}
                        >Please click on the link that has just been sent to your email account to verify your registration.</Typography>
                   <br /> Go to <Link to="/login" style={{textAlign:'left', marginTop:'15px'}}> Login</Link>
                  {'  | ' } Back to <span style={{color:'rgb(85, 26, 139)', textDecoration:'underline'}} onClick={backToForm}>register form</span>
                </div>
            </Paper>
        )
    }
    const backToForm = () => {
        setIsSuccessfulRegistred(prev => !prev);
    }
    const handleBlur =(e) =>{}

    return (       
        <div>
           
            <Grid container style={{height:'80vh'}} >
                <Grid item sm/>
                <Grid item sm className={classes.form} >
                { isSuccessfulRegistred ? verificationMsg() : (
                    <Paper style={{padding:'20px'}}>
                        <Typography variant="h2" className={classes.pageTitle}>
                            Sign Up
                        </Typography>
                            <Grid container className={classes.inputSection}   spacing={2}>
                                <Grid item sm={6} xs={12}>
                                    <TextField
                                            error={errors.firstName ? true : false}
                                            id="email"
                                            label="First Name"
                                            name="firstName"
                                            value={data.firstName}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            variant="outlined"
                                            style={{width:'100%'}}
                                            helperText={errors.firstName && errors.firstName}
                                            />
                                </Grid>
                                <Grid item sm={6} xs={12}>
                                    <TextField
                                            id="lastName"
                                            label="Last Name"
                                            name="lastName"
                                            value={data.lastName}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            variant="outlined"
                                            style={{width:'100%'}}
                                            error={errors.lastName ? true : false}
                                            helperText={errors.lastName && errors.lastName}
                                            />
                                </Grid>
                            </Grid>
                            <div className={classes.inputSection}>
                                <TextField
                                    id="email"
                                    label="Email"
                                    name="email"
                                    value={data.email}
                                    onChange={handleChange}
                                    variant="outlined"
                                    className={classes.textField}
                                    error={errors.email ? true : false}
                                    helperText={errors.email && errors.email}
                                     />
                            </div>
                            <div className={classes.inputSection}>
                                <TextField
                                    id="password"
                                    label="Password"
                                    name="password"
                                    value={data.password}
                                    onChange={handleChange}
                                    variant="outlined"
                                    className={classes.textField} 
                                    error={errors.password ? true : false}
                                    helperText={errors.password && errors.password}
                                    />
                            </div>   
                            <div className={classes.inputSection}>
                                <TextField
                                    id="password"
                                    label="Comfirm Password"
                                    name="password2"
                                    value={data.password2}
                                    onChange={handleChange}
                                    variant="outlined"
                                    className={classes.textField}
                                    error={errors.password2 ? true : false}
                                    helperText={errors.password2 && errors.password2}
                                    />
                            </div>     
                            <div className={classes.inputSection}>
                            <Button 
                                variant="outlined"
                                color="primary" 
                                size="medium" 
                                style={{width:'100%', marginTop:'15px'}}
                                 onClick={handleSubmit}
                                 >
                                Register
                            </Button>
                            </div>
                        <Link to="/login" style={{textAlign:'left', marginTop:'15px'}}>Login</Link>
                  </Paper>)}
                </Grid>
                <Grid item sm/>
            </Grid>
        </div>
    )
}

Signup.propTypes = {
    classes:PropTypes.object.isRequired,
    setAlert: PropTypes.func.isRequired,
    register:PropTypes.func.isRequired
}

 const mapStateToProps = state => {
    return {
        success: state.auth.success
    }
  } 
export default connect(
    mapStateToProps, 
    { setAlert, register}
)(withStyles(styles)(Signup));