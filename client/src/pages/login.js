import React,{useState, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import { setAlert } from '../redux/actions/alert'
import { login } from '../redux/actions/auth'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import {Grid} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';



const styles = {
    pageTitle:{
      marginBottom:'30px'
    },
    form:{
       
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        maxWidth:'400px'
        
    },
    textField:{
        marginBottom:'10px',
        width:'100%'
    }

};

const isValid = (errors) => {
    return Object.keys(errors).length > 0 ? false : true;
 } 
 

const  Login = ({classes, setAlert, login, isAuthenticated}) => {
    const [data, setData] = useState({
        email:'',
        password:''
    });
    const [errors, setErrors ] = useState({
        email:'',
        password:''
    });

    const handleSubmit = (e) =>{
        e.preventDefault();

        if(!isValid) return;
        // redux action
        login(data);

    }
    let history = useHistory();

    useEffect(() =>{
        if(isAuthenticated) {
            setAlert('Uspesno Logovanje', 'success', 2000);
            history.push('/');
        }
    },[isAuthenticated]) //eslint-disable-line

    const handleChange = (e) => {
        setData({...data, [e.target.name]: e.target.value});
        validation(e.target.name, e.target.value);
      
    }
    const validation = (field, value) => {
        switch(field) {
            case 'email':
                const regex =  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                const res =  regex.test(String(value).toLowerCase());
                !res ?  setErrors({...errors, email:'Mail is not valid'})
                : delete errors.email;
                break;
            case 'password':
                value === ''? 
                    setErrors({...errors, password: 'field is required'})
                    : delete errors.password;
                value.length < 3 ?
                    setErrors({...errors, password: 'min 3 characters'})
                    :delete errors.password;
                break;
            
            default:     
        }
    }

    return (       
        <div>
           
            <Grid container style={{height:'80vh'}} >
                <Grid item sm/>
                <Grid item sm className={classes.form} >
                    <Paper style={{padding:'20px'}}>
                    <Typography variant="h2" className={classes.pageTitle}>
                        Login
                    </Typography>
                    <form method="POST" noValidate onSubmit={handleSubmit}>
                 
                       <div className={classes.inputSection}>
                            <TextField
                               id="email"
                               label="Email"
                               name="email"
                               value={data.email}
                               onChange={handleChange}
                               variant="outlined"
                               className={classes.textField}
                               error={errors.email ? true : false}
                               helperText={errors.email && errors.email}
                                />
                      
                      </div> 
                      <div className={classes.inputSection}>
                            <TextField
                                id="password"
                                label="Password"
                                name="password"
                                value={data.password}
                                onChange={handleChange}
                                variant="outlined"
                                className={classes.textField} 
                                error={errors.password ? true : false}
                                helperText={errors.password && errors.password} />
                      
                      </div>     
                      <div className={classes.inputSection}>
                      <Button 
                            variant="outlined"
                            color="primary" 
                            size="medium" 
                            style={{width:'100%', marginTop:'15px'}}
                            type="submit"
                            >
                        Login
                        </Button>
 
                      </div>
                      <div className={classes.inputSection} style={{textAlign:'left', marginTop:'15px'}}>
                      <Link to="/signup" style={{textAlign:'left', marginTop:'15px'}}>Register</Link>
 
                      </div>
                       
                    </form>
                   
                    </Paper>
                   
                </Grid>
                <Grid item sm/>
              
            </Grid>
        </div>
    )
}

Login.propTypes = {
    classes:PropTypes.object.isRequired,
    setAlert: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired
}
const mapStateToProps = state => {
     return {
        isAuthenticated: state.auth.isAuthenticated
     }
};

export default connect(mapStateToProps, {setAlert,login})(withStyles(styles)(Login));