import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

import Layout from '../components/Layout';
import hero from '../assets/images/friends-hero.jpg';

const useStyles = makeStyles({
  card: {
    maxWidth:'100%',
    paddingTop:0
  },
  media: {
    height: 240,
    width:'100%',
    backgroundRepeat:'no-repeat',
    backgroundSize:'cover'
  },
});

export default function GroupLanding() {
  const classes = useStyles();

  return (
      <Layout style={{paddingTop:0}}>
            <Card className={classes.card}>
                <CardActionArea>
                
                    <CardMedia
                    className={classes.media}
                    image={hero}
              
                    />
                   
                </CardActionArea>
                <CardActions>
                    <Button size="small" color="primary">
                       Share
                    </Button>
                    <Button size="small" color="primary">
                        Learn More
                    </Button>
                </CardActions>
            </Card>
    </Layout>
  );
}