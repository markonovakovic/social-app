import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import './App.css';
import Alert from './components/UI/alert/Alert';
import Routes from './routes/index';
import { Provider }  from 'react-redux';
import store from './redux/store';
import setAuthToken from './utils/setAuthToken';
import { loadUser } from './redux/actions/auth';
//


if(localStorage.token) {
  setAuthToken(localStorage.token)
} 

function App() {

  useEffect(() =>{
    store.dispatch(loadUser());
  
  },[]);
  
  return (
    <Provider store={store}>
      <div className="App">
        <Alert />
        <Router>
          <Switch>
              <Routes />
          </Switch>
        </Router>
      </div>
    </Provider>
  
  );
}

export default App;
