const sharp = require('sharp');
const path = require('path');
const fs = require('fs');

/**
 * @params req - required, 
 */
module.exports = async (req, options) => {
    const { resize = 80, quality = 80, folderName, saveOriginal } = options;
    const { filename: image } = req.file;
    await sharp(req.file.path)
    .resize(resize)
    .jpeg({quality: quality})
    .toFile(
        folderName? path.resolve(req.file.destination,folderName,image): path.resolve(req.file.destination,image)
    );

    if(saveOriginal) fs.unlinkSync(req.file.path);
    console.log(req.file);

};