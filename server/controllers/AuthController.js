const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
/* const Joi =  require('@hapi/joi'); */
const { User } = require('../models/User');
const config = require('config');

/**
 * @route   POST api/auth/login
 * @desc    Login user
 * @access  Public
 */
exports.login = async(req, res) =>{
  
   /*   const  { error } =  scheme.validate(req.body); */
   /*   if (error)  return res.status(400).send(error.details[0].message); */

      const {  email, password  } = req.body;
      const user = await User.findOne({'email':email});
      
      if(!user) return res.status(400).send('You are not registred with this email');
      
      if(user.isActive === false) return res.status(400).send('User is not valid');
      
      const validPassword = await bcrypt.compare(password, user.password);

      if(!validPassword) return res.status(400).send('Invalid password');
       
      const token = jwt.sign({ _id: user._id }, config.get('jwtPrivateKey'),{
         expiresIn:"24h"
      });
      
        return res
                 .header('x-auth-token', token)
                 .send({token});
};

/**
 * @route   GET api/verify
 * @params  token
 * @desc    Send mail confiramtion register token
 * @access  Private
 */
exports.verifyProfile = async(req, res) =>{

   const user = await User.findOne({'token': req.query.token});
   if(!user) return res.status(404).send('bad request. Try it again!');

    user.token = null;
    user.isActive = true;
    user.save();
 
    return res.status(200).send({message:'Successfuly confirmation email'});
};

/**
 * @route   GET /auth/whoaim
 * @desc   Get data of logged user
 * @access  Private
 */
exports.getCurrentUser = async (req,res) => {
   try {
    const user = await User.findOne({'_id': req.user._id}).select('-token -password');
    if(!user) return res.status(400).send('User is not autentificated');

       return res.status(200).send(user);

   } catch (error) {
      return res.status(500).send('Server error');
   }
   
};
