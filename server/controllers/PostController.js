const { Post, Joi} = require('../models/Post');

exports.create = async(req, res) =>{
  try {
        const  { error }  =  Joi.validate(req.body);

        if(error) return res.status(400).send(error.details[0].message);
        const groupId = req.body.groupId ? req.body.groupId : null;
        const newPost = await new Post({
            user:req.user._id,
            groupId:groupId,
            content:req.body.content,
        });
        
        newPost.save();

        return res.status(201).send(newPost);
        
    } catch (error) {
        return res.status(500).send(error.details[0].message);
    } 
};

exports.single =  async (req, res) => {
    
    try {
         const post = await Post.findOne({'_id':req.params.id})
             .populate({path:'likes', select:'firstName lastName'})
             .populate({path:'comments.user', select:'firstName lastName'})
             .exec();
         if(!post) return res.status(400).send('This post doesn\'t exist!');
         return res.status(200).send(post);
 
     } catch (error) {
         return res.status(500).send('Server error');
     } 
 };

 exports.remove =  async (req, res) => {
    try {
         const post = await Post.findOne({'_id':req.params.id});
         if(!post) return res.status(400).send('This post doesn\'t exist!');

         if(post.userId.toString() != req.user._id) {
            return res.status(400).send('You don\'t have permissions to remove this post!');
         }

        await  Post.findOneAndDelete({'_id':req.params.id});
            
        return res.status(200).send('Post is successfully deleted');
 
     } catch (error) {
         return res.status(500).send('Server error');
     } 
 };

 exports.update =  async (req, res) =>{
 
     try {
        const post = await Post.findOne({'_id':req.params.id});
        if(req.user._id !== post.user.toString())  return res.status(400).send('you are not allowed to remove this post!');
   
        await Post.updateOne({'_id': req.params.id}, { $set: req.body });
   
        return res.status(200).send('Post is updated!');
         
     } catch (error) {
        return res.status(500).send('Error server');
     }
 };

exports.like = async(req, res) =>{
    
    try {

        const userId =  req.user._id;
        const post = await Post.findOne({'_id':req.params.id});
        let message = '';
        if(!post) return res.status(400).send('This post doesn\'t exist!');
          const index = post.likes.indexOf(userId);
        if(index !== -1) {
            post.likes.splice(index, 1);
            message =  'post is removed';
        } else { 
            post.likes.unshift(userId);
            message = 'post is added';
        }
    
        post.save(); 
     

        return res.status(200).send({message, like:userId});
    } catch (error) {
        return res.status(500).send(error.details[0].message);
    } 
    
};

exports.addComment =  async(req, res) =>{
    try {
        if(req.body.text === "" || req.body.text === undefined) return res.status(400).send('Message is empty!');

       const post = await Post.findOne({'_id': req.params.id});
        if(!post) return res.status(400).send('Post doesn\'t exist');

        post.comments.unshift({
            user: req.user._id,
            text: req.body.text
        });

        post.save(); 
        return res.status(200).send(post.comments);
    } catch (error) {
        return res.status(500).send(error.details[0].message);
    }
};

exports.updateComment =  async(req, res) =>{
    try {
       if(req.body.text === "" || req.body.text === undefined) return res.status(400).send('Message is empty!');
       const post = await Post.findOne({'_id': req.params.id});
       if(!post) return res.status(400).send('Post doesn\'t exist');
       
       const comment =  post.comments.find(item => item._id == req.params.commentId);
       if(!comment) return res.status(400).send('Comment doesn\'t exist!');
       if(comment.user.toString() !== req.user._id) return res.status(400).send('you are not allowed to remove this comment!');
        comment.text =  req.body.text;
        const index = post.comments.findIndex(comment => comment._id.toString() === req.params.commentId);
        post.comments.splice(index,1, comment);
     /*   post.comments.filter(item => item._id.toString() !== req.params.commentId); */
        post.save();  
        return res.status(200).send(post.comments);
    } catch (error) {
        return res.status(500).send(error.details[0].message);
    }
};

exports.removeComment =  async(req, res) =>{
    try {
       const post = await Post.findOne({'_id': req.params.id});
       if(!post) return res.status(400).send('Post doesn\'t exist');

       const comment =  post.comments.find(item => item._id == req.params.commentId);
       if(!comment) return res.status(400).send('Comment doesn\'t exist!');
       if(comment.user.toString() !== req.user._id) return res.status(400).send('you are not allowed to remove this comment!');
       const index = post.comments.findIndex(comment => comment._id.toString() === req.params.commentId);
       if(index !== -1) {
           post.comments.splice(index, 1);
       }
        post.save();  
        return res.status(200).send(post.comments);
    } catch (error) {
        return res.status(500).send(error.details[0].message);
    }
};