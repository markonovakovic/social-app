const NotificationService = require('../services/NotificationService');

exports.unreadNotifications = async(req, res) => {

    const {status, data } = await NotificationService.fetchUnreads(req.user._id);
    res.status(status).send(data);
};

exports.seenNotification = async(req, res) => {
    const {status, data } = await NotificationService.updateStatus(req.params.id, 'seen');
    res.status(status).send(data);
};