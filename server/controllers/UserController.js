const UserService = require('../services/UserService');
const PostService = require('../services/PostService');
const { Joi, } = require('../models/User');
const pagintaion =  require('../helpers/paginatiton-params');
const escapeRegex = require('../helpers/regex-escape');
const Mailer = require('../config/mailer');
const htmlTemplate = require('../helpers/mail-templates');


exports.getUsers = async (req, res) => {

    const params =  {};
    params.page = Number(req.query.page) || 1;
    params.perpage = Number(req.query.perpage) || 3; 
    params.search = null;
    if(req.query.search) {
       // const searchQuery = req.query.search;
        params.search = new RegExp(escapeRegex(req.query.search), 'gi');
    }

    try {
        const users = await UserService.fetchUsers(params);
        return res.status(users.status).json(users);
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
};

exports.getUser = async (req, res) =>{

   const {status, data} = await UserService.fetchUser({slug:req.params.slug});
   return res.status(status).send(data);
};


exports.suggestedFriends = async(req, res) => {
    const params = pagintaion(req, 4);
    const {status, data} = await UserService.suggested(req.user._id, params);
    return res.status(status).send(data);
};

/**
 * @route   POST api/users/create
 * @desc    Create new user
 * @access  Public
 */
exports.createUser = async(req, res) =>{
    
    const  { error } =  Joi.validate(req.body);
    if (error)  return res.status(400).send(error.details[0].message);
    const user = await UserService.fetchUser({email:req.body.email});
    
    if(user.data) return res.status(400).send('User is alredy exist');
    const{status, data }  = await UserService.create(req.body);
    const templateData =  {
        name:data.firstName,
        token:data.token
    };
    const mailOptions =  {
        from:'marko.novakovic81@gmail.com',
        to: data.email,
        subject:'Social app. Comfirm your acount',
        html: htmlTemplate.verifyRegisterToken(templateData)
    };

    Mailer.sendMail(mailOptions,function(err,data) {
         if(err) {
             console.log(err,'greska');
         } else {
             console.log(data);
         }
    });


    return res.status(status).send(data);

};

exports.getPostsByUser = async (req, res) => {
  //  const userId = req.params.id ? req.params.id : req.user._id;
    const { slug } = req.params;
    const params  =  pagintaion(req, 5);
    const {data: user} =  await UserService.fetchUser({slug});

    const query = {user:user._id,groupId:null};

    const result = await PostService.fetchPosts(query, params);

    return res.status(result.status).send(result.data);
};

exports.getGroupsByUser = async(req, res) => {
                   
   // const user = await User.findById(req.user._id);
   const query = {
       slug: req.params.slug
   };
    const {status, data} = await UserService.fetchGroups(query);
    

   return res.status(status).send(data);
  
   
 };


exports.updateUser = async (req, res) => {

  const  { status, data } = await UserService.update(req);

  return res.status(status).send(data);
};
 
exports.sendFriendshipRequest =  async (req, res) => {
    const {status, data} = await UserService.requestFriend(req.user._id, req.params.slug);
    return res.status(status).send(data);
};

exports.acceptFriendRequest =  async (req, res) => {
   await UserService.requestFriend(req.user._id, req.params.slug);
   return res.status(200).send('friendships has been accepted');

};

exports.removeFriend = async (req, res) => {
    const data = await UserService.removeFriend(req.user._id, req.params.slug);
    return res.status(data.status).send(data.data);

};

exports.getFriendsByUser = async(req,res) => {

      const { slug  } = req.params;
      const friendshipStatus =   req.query.status || 'all';
      const {status, data}=  await UserService.fetchFriends(slug,friendshipStatus);

      return res.status(status).send(data);

  
};
