//const { User } = require('../models/User');
const { Post } = require('../models/Post');
const {Group, Joi } = require('../models/Group');
const groupService = require('../services/GroupService');
const postServices = require('../services/PostService');
const userService = require('../services/UserService');
const paginationParams = require('../helpers/paginatiton-params');



/**
 * @route   Post api/groups/create
 * @desc    Register new Group
 * @access  Private
 */
exports.create = async(req, res) => {

  const  { error }  =  Joi.validate(req.body);
  if(error) return res.status(400).send(error.details[0].message);
  const data =  await groupService.create(req.user._id, req.body);
  
  return res.status(data.status).send(data.data);
};

/**
 * @route   GET api/groups/:id
 * @desc    Get Single Group
 * @access  Private
 */

exports.getGroup =  async(req, res) => {
  const groupId =  req.params.id;

  try {
    const group = await Group.findById(groupId);
    if(!group) return res.status(404).send('Group is not exist!');
      return res.status(200).send(group);
  } catch (error) {
      return res.status(500).send(error.details[0].message);
  }
};


/**
 * @route   GET  ?
 * @desc     fetch all groups which belongs selected group
 * @access  Private
 */

exports.getPostsByGroup = async(req, res) => {
  const groupId = req.params.id;
  const params = paginationParams(req, 4);
  const query =  { 'groupId': groupId};
  const { status, data} = await postServices.fetchPosts(query, params);

  return res.status(status).send(data);
};

/**
 * @route   DELETE api/groups/:id
 * @desc     Remove Group
 * @access  Private
 */
exports.removeGroup =  async(req, res) => {
  const groupId =  req.params.id;
  try {
    const group = await Group.findById(groupId);
    if(!group) return res.status(404).send('Group is not exist!');
    
    if(group.createdBy  !== req.user._id) return res.status(400).send('You don\'t have permission to remove this group');
       await Group.findOneAndDelete({'_id':groupId});
      return res.status(200).send('Group has successfully deleted');

  } catch (error) {
    return res.status(500).send(error.details[0].message);
  }
 
};


/**
 * @route   PATCH api/groups/:id
 * @desc     Remove Group
 * @access  Private
 */
exports.updateGroup =  async(req, res) => {
  const groupId =  req.params.id;
  try {

    const group = await Group.findById(groupId);
    if(!group) return res.status(404).send('Group is not exist!');
    if(group.createdBy  != req.user._id) return res.status(400).send('You don\'t have permission to change group');
    
    await Group.updateOne({'_id':groupId}, {
        $set:req.body
    });

      return res.status(200).send('Successfully updated');
      
  } catch (error) {
    return res.status(500).send(error.details[0].message);
  }
 
};




/**
 * @route   POST api/groups/:id/posts/create
 * @desc    Add New Post on Group
 * @access  Private
 */
exports.createPost =  async(req, res) =>{

   try {
   
    const newPost = new Post({
      user:req.user._id,
      groupId: req.params.id,
      content:req.body.content
    });
    newPost.save();
    return res.status(201).send(newPost);
     
   } catch (error) {
    return res.status(500).send(error.message);
   }
};


/**
 * @route   POST api/groups/:id/request
 * @desc    Send Join Request to Group Owner
 * @access  Private
 */
exports.sendJoinRequest = async(req, res) => {
  const data = await groupService.sendRequest(req.params.id, req.user._id);
  return res.status(data.status).send(data.data);
};

/**
 * @route   POST api/groups/:id/approve-join
 * @desc    Aprove Join to Group Request 
 * @access  Private
 */
exports.approveJoinGroupRequest = async(req, res) => {
  const data = await groupService.approveRequest(req.user._id, req.params.id);
  return res.status(data.status).send(data.data);
};

/**
 * @route   POST api/groups/:id/reject
 * @desc    Reject Join to Group Request 
 * @access  Private
 */
exports.rejectRequest = async(req, res) => {
  const data = await userService.removeFriend(req.user._id, req.params.slug);
  return res.status(data.status).send(data.data);
};


/**
 * @route   POST api/groups/:id/leave
 * @desc    Reject Join to Group Request 
 * @access  Private
 */
exports.leaveGroup =  async(req, res) => {

  const data = await groupService.leaveGroup(req.user._id, req.params.id);
  return res.status(data.status).send(data.data);
};


/**
 * @route   GET api/groups/suggested
 * @desc    Suggesteed Groups
 * @access  Private
 */
exports.groupSuggested = async(req,res) => {
  const data = await groupService.suggested(req.user._id, req.params.id);
  return res.status(data.status).send(data.data);
};

/*  TODO
exports.groupsByUser = async(req, res) => {

   const user = await User.findById(req.user._id);
   
   const groupKeys =  user.groups.map(item => item.group )
   const groups = await Group.find({'_id':{
    $in : groupKeys
     }});
  return res.status(200).send(groups);
 
  
}; */