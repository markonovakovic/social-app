const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const AuthController = require('../controllers/AuthController');
const groupController = require('../controllers/GroupController.js');
const PostController = require('../controllers/PostController');
const UserController = require('../controllers/UserController');
const NotificationController = require('../controllers/NotificationController');


/*******
 * auth
 ******/
router.post('/auth', AuthController.login);
router.get('/auth/whoaim', auth, AuthController.getCurrentUser);
router.get('/verify', AuthController.verifyProfile);


/*********
 * groups
 ********/
//router.get('/groups/my-groups',  auth, groupController.groupsByUser);
router.get('/groups/suggested', auth, groupController.groupSuggested);
router.get('/groups/:id', auth, groupController.getGroup);
router.post('/groups/:id/request', auth, groupController.sendJoinRequest);
router.post('/groups/:id/approve-join', auth, groupController.approveJoinGroupRequest);
router.post('/groups/:id/reject', auth, groupController.rejectRequest);
router.delete('/groups/:id', auth, groupController.removeGroup);
router.post('/groups/create', auth, groupController.create);
router.patch('/groups/:id', auth, groupController.updateGroup);
router.post('/groups/:id/posts/create', auth, groupController.createPost);
router.post('/groups/:id/leave', auth, groupController.leaveGroup);





/******
 * posts
 *****/

router.post('/posts/create', auth, PostController.create);
router.post('/posts/:id/like',  auth, PostController.like);
router.get('/posts/:id', auth, PostController.single);
router.delete('/posts/:id/remove', auth, PostController.remove);
router.patch('/posts/:id/update', auth,  PostController.update);
router.patch('/posts/:id/comments/add', auth,  PostController.addComment);
router.patch('/posts/:id/comments/:commentId/remove', auth,  PostController.removeComment);
router.patch('/posts/:id/comments/:commentId/update', auth,  PostController.updateComment);


/**
 * users
 */

const multer = require('multer');

const storage = require('../config/upload-config');
const upload = multer(storage);


router.get('/users', auth, UserController.getUsers);
router.get('/users/:slug', auth, UserController.getUser);
router.get('/users/:slug/posts', auth, UserController.getPostsByUser);
router.get('/users/:slug/groups', auth, UserController.getGroupsByUser);
router.get('/users/:slug/friends', auth, UserController.getFriendsByUser);
router.get('/suggested-friends', auth, UserController.suggestedFriends);
router.post('/users/create', UserController.createUser);
router.patch('/users/update',upload.single('avatar'), auth, UserController.updateUser);
router.patch('/users/friendship/:slug/request', auth , UserController.sendFriendshipRequest);
router.patch('/users/friendship/:slug/accept', auth , UserController.acceptFriendRequest);
router.patch('/users/friendship/:slug/remove', auth , UserController.removeFriend);


/***************
 * Notifications
 ***************/


router.get('/notifications/unread', auth, NotificationController.unreadNotifications);
router.patch('/notifications/:id/seen', auth, NotificationController.seenNotification);

module.exports = router;