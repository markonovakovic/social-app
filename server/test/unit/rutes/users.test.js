const lib = require('../../../helpers/paginatiton-params');
test('pagination params', () => {
   const res =  lib({ query:{perpage:4, page:1}},2)
   const params = {perpage:4, page:1};
   console.log(res);
    expect(res).toEqual( { page: 1, perpage: 4, search: null });
    expect(res.search).toBeNull();
} )

