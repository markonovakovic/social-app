const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
require('../config/db');


const notificationSchema = new mongoose.Schema({
    sender:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    reciver:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    message:{
        type:String,
        required:true
    },
    type: {
        type:String,
        enum:["standard","accepted_friendship","message","accept_new_member"],
        default:"standard"
    },
    isSeen: {
        type: Boolean,
        default:false
    },
    createdAt:{
        type:Date,
        default: Date.now()
    },
    updatedAt: {
        type:Date,
        default: Date.now()
    }
});

const Notification  =  mongoose.model('Notification', notificationSchema);

/* const validate =  Joi.object({
    user: Joi.string(),
    groupId: Joi.string(),
    content:Joi.string().min(3).required(),
    comments: Joi.array()
}); */

exports.Notification =  Notification;
