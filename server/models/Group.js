const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
require('../config/db');


const groupSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true,
        trim:true,
        minlength:3,
        maxlength:150
    },
    slug:{
        type:String,
        required:true,
        unique:true,
        minlength:3,
        maxlength:150
    },
    description: {
        type:String
    },
    createdAt:{
        type:Date,
        default: Date.now()
    },
    createdBy:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    isActive: Boolean,
    imageUrl:{
        type:String
    }, 
    
});


const Group  =  mongoose.model('Group', groupSchema);

const validate =  Joi.object({
    name: Joi.string().min(3).max(150).required(),
 /*    slug:Joi.string().min(3).max(150).required(), */
    description:Joi.string().min(3),
    isActive : Joi.bool()
});

exports.Group =  Group;
exports.Joi = validate;