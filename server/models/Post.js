const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
require('../config/db');


const postSchema = new mongoose.Schema({
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    groupId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Group',
        default:null
    },
    content:{
        type:String,
        required:true,
        minlength:3,
    },
    createdAt:{
        type:Date,
        default: Date.now()
    },
    comments:[{
        id: String,
        user:{ type:mongoose.Schema.Types.ObjectId,
        ref:'User',
        },
        text:{
          type:String
        },
        createdAt:{
            type: Date,
            default: Date.now()
         }
       }
    ],
    likes:[
        
        { 
            type:mongoose.Schema.Types.ObjectId,
            ref:'User'
        }  
       
    ],
    status: {
        type:String,
        enum:["publish","draft","peding"],
        default:"publish"
    },
    thumbnail:{
        type:String,
        maxlength:300
    }, 
    
});


const Post  =  mongoose.model('Post', postSchema);

const validate =  Joi.object({
    user: Joi.string(),
    groupId: Joi.string(),
    content:Joi.string().min(3).required(),
    comments: Joi.array()
});

exports.Post =  Post;
exports.Joi = validate;