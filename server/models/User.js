const mongoose = require('mongoose');
//const friends = require("mongoose-friends")
const Joi = require('@hapi/joi');
require('../config/db');

const userSchema = new mongoose.Schema({
    slug: {
      type: String,
      required:true,
      unique:true,
      minlength:2,
      maxlength:256
    },
    firstName:{
        type:String,
        required:true,
        minlength:2,
        maxlength:50
    },
    lastName:{
        type:String,
        required:true,
        minlength:2,
        maxlength:50
    },
    email:{
        type:String,
        required:true,

    },
    avatar:{
        type:String,
        required:false
    },
    phone:{
       type:String,
       minlength:2,
       maxlength:100,
       required:false,
    },
    born: {
        type:Date,
        required:false
    },
    bio: {
        type:String,
        required: false,
        maxlength:2500
    },
    address: {
        type:String,
        minlength:2,
        maxlength:250,
        required:false
    },
    city: {
        type:String,
        minlength:2,
        maxlength:250,
        required:false
    },
    hobies:[ {type:String} ],
    password:String,
    createdAt:{ 
        type:Date,
        default:Date.now()
    },
    token:String,
    isActive: Boolean,
    groups:[{
       _id : false,
       group: {
        type:mongoose.Schema.Types.ObjectId,
        ref:'Group'
       },
       role:{
           type:String,
           enum:["member","moderator","admin"],
           default:"member"
       },
       status:{
        type:String,
        enum:["requested","banned","active","none"],
        default:"none"
       },
       isActive: {
           type:Boolean,
           default: false
       },
       date: {
        type:Date
      }
    }],
    friends:[
        {
            _id : false,
            user:{ 
              type:mongoose.Schema.Types.ObjectId,
              ref:'User',
            },
            status:{
                type:String,
                enum:["pending","accepted","requested"],
                default:"pending"
            },
            date:{
                type: Date,
                default: Date.now()
                }
            }
    ]
});

//userSchema.plugin(friends());
const User = mongoose.model('User', userSchema);
/* 

function validateUser(user){
    const schema = {
        firstName: Joi.string().min(2).max(50).required(),
        lastName: Joi.string().min(2).max(50).required(),
        email:Joi.string().email().required(),
        password: Joi.string().required(),
        avatar: Joi.string().max(255),
        phone : Joi.string().max(255),
        token : Joi.string().max(255),
        isActive : Joi.bool()

    }

    return Joi.validate(user, schema)
} */

const validate =  Joi.object({
    firstName: Joi.string().min(2).max(50).required(),
    lastName: Joi.string().min(2).max(50).required(),
    email:Joi.string().email().required(),
    password: Joi.string().required(),
    avatar: Joi.string().max(255),
    phone : Joi.string().max(255),
    token : Joi.string().max(255),
    isActive : Joi.bool()
});

exports.User =  User;

exports.Joi = validate;