const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const config = require('config');
const winston = require('winston');
const cors = require('cors');

require('express-async-errors');
require('dotenv').config();

app.use(bodyParser.json());
app.use(cors());

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      //
      // - Write to all logs with level `info` and below to `combined.log` 
      // - Write all logs error (and below) to `error.log`.
      //
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'combined.log' })
    ]
  });


if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
      format: winston.format.simple()
    }));
}

if(!config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
}
app.use('/api/images',express.static('images'));
app.use('/api', require('./routes/api'));


const port = process.env.PORT || 5000;


app.listen(port, () => console.log(`App listening on port ${port}!`));

