const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
    service: 'gmail',
    //host:'process.env.EMAIL_HOST',
    //port:process.env.EMAIL_PORT,
    //secure: false, // true for 465, false for other ports
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD
    },
    /* tls:{
        rejectUnauthorized:false
      } */
});

module.exports = transport;