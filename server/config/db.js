const mongoose = require('mongoose');
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true ,
    useCreateIndex: true
};

mongoose.connect(process.env.MONGODB, options)
         .then(() => console.log('Connected to MondoDb...'))
         .catch(err => console.error('Could not connect to MongoDb', err));
