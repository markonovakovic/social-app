const {Post} = require('../models/Post');
const apiResponse = require('../helpers/api-response');

exports.fetchPosts =  async(query, params) => {
    const { page, perpage, search } = params;

    let posts = null;
    let posts_total = 0;
    try {
        if(search) {
        posts = await Post.find(query)
                .where({'content': search})
                .populate({path:'user',select:'firstName lastName _id avatar'})
                .populate({path:'comments.user',select:'firstName lastName _id avatar'})
                .populate({path:'likes',select:'firstName lastName _id avatar'})
                .sort({'createdAt':-1})
                .skip((perpage * page) - perpage)
                .limit(perpage);

                posts_total = await Post.countDocuments(query)
                                        .where({'content': search});
        
        
        } else {
            posts = await Post.find(query)
                .populate({path:'comments.user',select:'firstName lastName _id avatar'})
                .populate({path:'likes',select:'firstName lastName _id avatar'})
                .populate({path:'user',select:'firstName lastName _id avatar'})
                .sort({'createdAt':-1})
                .skip((perpage * page) - perpage)
                .limit(perpage);

                posts_total = await Post.countDocuments(query);
        }
    
        const response = {
            posts,
            posts_total,
            page,
            pages_total: Math.ceil(posts_total /  perpage),
            perpage
        };
    
        return apiResponse(200, response);

    } catch (error) {
        return apiResponse(500, error);
    }
};
 