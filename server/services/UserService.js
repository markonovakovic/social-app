const { User } = require('../models/User');
const { Group } = require('../models/Group');
const { Notification } = require('../models/Notification');
const bcrypt = require('bcrypt');
const apiResponse =  require('../helpers/api-response');
const slug = require('slug');
const ImgResizer = require('../helpers/resize-image');

exports.fetchUsers = async function (params) { // eslint-disable-line
  const { page, perpage, search } = params;
     let users  = null;
     let pages = 0;
 
    try {
       
        if(search) {
            users = await User.find({'firstName': search})
              .where({'isActive':true})
              .skip((perpage * page) - perpage)
              .limit(perpage)
              .select(['-password', '-token']);

              pages =   await User.count({'firstName': search});
        }else {
           users = await User.find({})
              .where({'isActive':true})
              .skip((perpage * page) - perpage)
              .limit(perpage)
              .select(['-password', '-token']);
              pages =   await User.count({});
        }
        
        const response = {
            users,
            pages:pages
        };

        return apiResponse(200,response);
    } catch (e) {
        // Log Errors
        throw Error('Error while Paginating Users');
    }
};
exports.fetchUser = async (query) => {

    try {
        const users = await User.findOne(query)
            .select(['-password', '-token','-groups','-friends'])
            .exec(); 
    
  
        return apiResponse(200,users);
    } catch (e) {
        // Log Errors
        throw Error('Error while Paginating Users');
    }
};

exports.create =  async (data) => {

     // crypt password
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    const hashed = await bcrypt.hash(data.password, salt);
    const token = await bcrypt.hash(data.email + new Date(), salt);

    // check full name and create slug
    let firstName =  new RegExp(data.firstName, 'i');
    let lastName =  new RegExp(data.lastName, 'i');

    const countUsersWithSameFullName = await User.countDocuments({ $and: [ { 'firstName': firstName }, { 'lastName': lastName } ] });

    firstName = slug(data.firstName.toLowerCase(),'-');
    lastName =  slug(data.lastName.toLowerCase(),'-');
     
     const newSlug = countUsersWithSameFullName != 0 ? `${firstName}-${lastName}-${(countUsersWithSameFullName + 1)}`:`${firstName}-${lastName}`;
     

    try {
        const newUser =  new User({
                slug : newSlug,
                firstName:data.firstName,
                lastName:data.lastName, 
                email:data.email,
                address:null,
                city:null,
                password:hashed, 
                token:token,
                isActive: false,
                cratedAt: new Date()

        });
        newUser.save();
        return apiResponse(200,newUser);
    } catch (error) {
        return apiResponse(500,error);
    }
};

exports.update =  async(req) => {
     
      await User.updateOne(
            {'_id':req.user._id},
            {$set:req.body}     
        );

        const user = await User.findOne({'_id':req.user._id});
        if(req.file) {
          
            ImgResizer(req,{size:85, folderName:'avatars', saveOriginal:true});
            const imageUrl =  'api/images/avatars/'+req.file.filename;
            user.avatar =  imageUrl;
            user.save();
        }

        return apiResponse(200,user);
    
};
// this method fetch all groups for specific user
exports.fetchGroups = async(query) => {

    const user = await User.findOne({'slug': query.slug})
                        .populate({path:'groups.group',select:'name  _id description createdBy imageUrl'});

   const groups = user.groups.filter(group =>  group.status !== 'banned');

   //if(groups.length === 0)  return apiResponse(400,'You are not member of any group');
                       
  /*  second way how we can fetch groups that belongs specific user
    const groupKeys =  user.groups.map(item => item.group );

    const groups = await Group.find({'_id':{
     $in : groupKeys
      }}); */

    return apiResponse(200,{groups});
  
 };

exports.fetchFriends = async(friendSlug, status) => {

  console.log(status);
   try {
      
        
         const result = await User.findOne({'slug':friendSlug})
             .where({'isActive':true})
             .populate({path:'friends.user',select:'_id firstName lastName avatar slug email'})
             .select(['friends']);
            // return apiResponse(200,result);
           /*  
           const friends = result.friends.filter(friend =>{
              if(status === 'all') {
                   return friend.status ==='accepted';
               } else {
                   return friend.status === status;
               } 
              
           }); */
        const data = result.friends  || [];
       return apiResponse(200,data);
   } catch (e) {
       // Log Errors
       console.error(e);
       throw Error('Error while Paginating Users');
   }
};

exports.suggested = async(userId, params) => {
    const { page, perpage, search } = params;

    let suggested_friends =  null;
    let friends_total = 0;
    try {
     if(search) {
        suggested_friends = await User.find({'firstName':{
            $ne : userId
         }})
        .or([{'firstName':search},{'lastName': search }])
        .where({'friends.user': {$ne: userId}})
        .skip((perpage * page) - perpage)
        .limit(perpage)
        .select('_id slug firstName lastName email avatar');

        friends_total =  await User.countDocuments({'firstName':{
            $ne : userId
         }})
         .or([{'firstName':search},{'lastName': search }])
         .where({'friends.user': {$ne: userId}});

     } else {

        suggested_friends = await User.find({'_id':{
            $ne : userId
             }})
             .where({'friends.user': {$ne: userId}})
             .skip((perpage * page) - perpage)
             .limit(perpage)
             .select('_id slug firstName lastName email avatar');
        
             friends_total = await User.countDocuments({'_id':{
                $ne : userId
                 }})
                 .where({'friends.user': {$ne: userId}});
     }
         suggested_friends =  suggested_friends.map(friend => {
             return { status:null, user: friend._doc};
         });
        const response = {
            suggested_friends,
            friends_total,
            page,
            pages_total: Math.ceil(friends_total /  perpage),
            perpage
        };
     /*    response.users = users;
        response.friends_total  = friends_total; */
        return apiResponse(200,response);
    } catch (error) {
        console.error(error);
        throw Error('Error while Paginating Users', error);
    }
};
exports.requestFriend = async(userOneId, userTwoSlug) => {
 
    const user1 = await User.findOne({'_id':userOneId});
    const user2 =  await User.findOne({'slug': userTwoSlug});

    const userTwoId = user2._id;
    const index1 = user1.friends && user1.friends.findIndex(friend => friend.user.toString() == userTwoId);
   if(index1 == -1) {
        user1.friends.unshift({user:userTwoId, date: Date.now(), status:'requested'});
        user1.save();
    } else{
        if(user1.friends[index1].status === 'pending') {
            user1.friends[index1].status = 'accepted';
            user1.save();
            const notification = await new Notification({
                sender:user1._id,
                reciver:user2._id,
                message:`${user1.firstName} ${user1.lastName} has accepted your friendship request`,
                type:'standard'
            });
            notification.save();
        } else {
           user1.friends.splice(index1,1);
           user1.save();
        }
    }

    const index2 = user2.friends && user2.friends.findIndex(friend => friend.user.toString() == userOneId);
    if(index2 == -1) {
        user2.friends.unshift({user:userOneId, date: Date.now(),status:'pending'});
        user2.save();
        const notification = await new Notification({
            sender:user1._id,
            reciver:user2._id,
            message:`${user1.firstName} ${user1.lastName} sent you friendship request `,
            type:'accepted_friendship'
        });
        notification.save();

       // return apiResponse(200,{message:'succesfully send friendship request!'});
    }else {
        if(user2.friends[index2].status === 'requested') {
            user2.friends[index2].status = 'accepted';
            user2.save(); 
            return apiResponse(200,{message:'accepted friendship!'});
        } else {
            user2.friends.splice(index2,1);
            user2.save(); 
        }
    }
    return apiResponse(200,{message:'success!!!'});
};
exports.removeFriend =  async(userIdOne, userTwoSlug) =>{
    try {
        const userIdTwo =  (await User.findOne({'slug':userTwoSlug}).select('_id')).exec();
        await User.updateOne(
            {'_id':userIdOne},
            {$pull: { 'friends':{user: userIdTwo}}}
        );
    
        await User.updateOne(
            {'slug':userTwoSlug},
            {$pull: { 'friends':{user: userIdOne}}}
        );
        return apiResponse(200,'User has been removed!');
          
      } catch (error) {
         return apiResponse(500,error);
      }
};



exports.fetchGroup = async(req, res) => {

    const user = await User.findById(req.user._id);
    
    const groupKeys =  user.groups.map(item => item.group);
    const groups = await Group.find({'_id':{
     $in : groupKeys
      }});
   return res.status(200).send(groups);
 };
 