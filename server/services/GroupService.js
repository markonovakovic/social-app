const { User } = require('../models/User');
const { Group } = require('../models/Group');
const { Notification } = require('../models/Notification');
const slug = require('slug');
const apiResponse =  require('../helpers/api-response');

exports.getByName  = async function(name) {
    const group = await Group.find({'name': name});
    if(!group) {
        return apiResponse(404,"Group doesn't exist!");
    }
    return apiResponse(200,group);
};
exports.create = async function (userId, data) { 
   // const res = {};
    try {
       
        const group = await Group.findOne({'name':data.name});
        if(group) return apiResponse(400,'Group is alredy exist e');
        const newGroup =  new Group();
            newGroup.name = data.name;
            newGroup.slug = slug(data.name.toLowerCase(),'-');
            newGroup.description = data.description;
            newGroup.isActive =  true;
            newGroup.createdBy = userId;
            newGroup.createdAt = new Date();
            newGroup.save();
         
        const user = await User.findById(userId);
            user.groups.push(newGroup._id);
            user.save();
  
        return apiResponse(201,newGroup);
    } catch (e) {
        // Log Errors
        throw Error('Error');
    }
};

exports.getGroupsByUserId = async function(userId) {
 console.log(userId);
};


exports.sendRequest =  async(groupId,userId) => {
    const user = await User.findOne({'_id':userId});
    if(!user)  return  apiResponse(200,'User doesn\'t exist');

     const groupData = {
        group:groupId,
        role:'member',
        status:'requested',
        isActive:false,
        date:new Date()
    
     };
   
     const index =   user.groups.findIndex(item => item.group == groupId);
     //whitdorw request
     if(index !== -1) {
         user.groups.splice(index, 1);
         user.save();
         return  apiResponse(200, groupData);
     }
     user.groups.push(groupData); 
     user.save();
     
     // SENT NOTIFICATION TO GROUP'S CREATOR
     const group =  await Group.findOne({"_id": groupId});

     const notification = new Notification({
        sender:userId,
        reciver:group.createdBy,
        message:`${user.firstName} ${user.lastName} sent join request to group ${group.name} `,
        type:'accept_new_member'
     });
     notification.save()

    return  apiResponse(200 ,groupData);
};

exports.approveRequest =  async(userId, groupId) => {

  
    const user = await User.findOne({'_id':userId});
    if(!user)  return  apiResponse(400,'User doesn\'t exist');

    const group =  await Group.findOne({"_id": groupId});
    if(!group)   return  apiResponse(403, 'This group doesn\'t exist');


    const groupIndex = user.groups.findIndex(item => item.group === groupId);
    const selectedGroup = user.groups.find(item => item.group == groupId);

    if(String(group.createdBy) != userId  && selectedGroup.role !== 'moderator' &&  selectedGroup.role !== 'admin') {
        return  apiResponse(403, 'You don\'t have permmision to change status of members');
    } 
    
    selectedGroup.status = 'active';
    selectedGroup.isActive = true;
    selectedGroup.date = new Date();
   
    user.groups[groupIndex] = selectedGroup;

    user.save();
 
    return  apiResponse(200,{group:selectedGroup});
};


exports.leaveGroup = async(userId, groupId) => {

    const user = await User.findById(userId);
    const index = user.groups.indexOf(groupId);
  
    if(index === -1) {
        return  apiResponse(404,'Group is not exist!');
    }
  
     user.groups =  user.groups.filter(item => item.group !== groupId);
     user.save();
     return  apiResponse(200,'Successfully leave group');
};


exports.suggested = async (userId) => {

const user = await User.findById(userId);
   
const groupKeys =  user.groups.map(item => item.group);
const groups = await Group.find({'_id':{
                              $nin : groupKeys
                         }})
                        .populate({path:'createdBy',select:'firstName lastName _id'});
 const res = [];
 groups.forEach(group => {
     res.push({group:group, status:null, isActive:false});
 });
    return  apiResponse(200,res);
};