const { Notification } = require('../models/Notification');
const apiResponse =  require('../helpers/api-response');


exports.fetchUnreads = async(userId) => {
     const notifications = await Notification.find({reciver: userId})
                  /*   .where({'isSeen':false}) */
                    .populate({path:'sender',select:'firstName lastName avatar -_id'})
                    .populate({path:'reciver',select:'firstName lastName avatar -_id'})
                    .sort({createdAt:-1})
     return apiResponse(200, notifications)
};

exports.updateStatus = async(notificationId, status) => {

   
     try {
        const notification = await Notification.findById(notificationId);
        notification.isSeen = true,
        notification.updatedAt = Date.now();
        notification.save();
        
         const response = {
             notification,
             message:'Notification has been seen!'
         }
        
            removeNotification(notificationId)
        
         return apiResponse(200, response)
     } catch (error) {
        return apiResponse(500, error)
     }
};

 function removeNotification( notificationId) {
    const expireTime = 1000 * 60 * 60 * 24 * 2;
    setTimeout(() => {
         Notification.findOneAndDelete({_id:notificationId});
    },expireTime);
    
  
}